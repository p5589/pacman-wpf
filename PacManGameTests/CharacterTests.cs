using NUnit.Framework;
using PacManGame;

namespace PacManGameTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void PlayerLives()
        {
            Player player = new Player();
            Assert.AreEqual(player.Lives, 3);
        }

        [Test]
        public void PlayerGetsDamagedByGhost()
        {
            Player player = new Player();
            Ghost ghost = new Ghost("Red");
            ghost.DoDamage(player);
            Assert.AreEqual(player.Lives, 2);
        }

        [Test]
        public void PlayerGetsDamagedByHans()
        {
            Player player = new Player();
            HansGhost hans = new HansGhost();
            hans.DoDamage(player);
            Assert.AreEqual(player.Lives, 1);
        }
    }
}