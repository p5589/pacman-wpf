﻿using NUnit.Framework;
using PacManGame;
using System;

namespace PacManGameTests
{
    class HighscorePlusTests
    {
        private Highscore myHighscore;

        [SetUp]
        public void Setup()
        {
            myHighscore = new Highscore();
        }

        [Test]
        public void HighscoreDateTest()
        {
            myHighscore.Date = DateTime.Now;

            Assert.AreEqual(myHighscore.OnlyDate, DateTime.Now.ToString("dd-MM-yyyy"));
        }
    }
}
