﻿using NUnit.Framework;
using PacManGame;
using System.Collections.Generic;

namespace PacManGameTests
{
    class AnimationTests
    {
        private Animation myAnimation;

        [SetUp]
        public void Setup()
        {
            myAnimation = new Animation();
        }

        //ACTUAL TESTS WAW!

        [Test]
        public void DictionaryTotalTest()
        {

            //Test GetDictionary
            Dictionary<string, Animation> myDictionary = myAnimation.GetDictionary();
            if (myDictionary.Count == 29)
            {
                Assert.AreEqual(myDictionary.Count, 29);
            }
        }

        [Test]
        public void DictionaryEntryTest()
        {
            //Test TryGetValue nad Containskey

            Dictionary<string, Animation> myDictionary = myAnimation.GetDictionary();
            if (myDictionary.TryGetValue("RedUp", out myAnimation))
            {
                if (myAnimation.Dictionary.ContainsKey("RedUp"))
                {
                    Assert.Pass();
                }
            }
        }
    }
}
