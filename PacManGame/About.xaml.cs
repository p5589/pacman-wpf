﻿using System;
using System.Windows;

namespace PacManGame
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Window
    {
        public About()
        {
            InitializeComponent();

            lblTitleRules.Content = "Rules:";
            lblRules.Content = "You control Pac-Man, who must eat all the dots inside an enclosed maze while avoiding" + Environment.NewLine + "the ghosts. Eating large dots called Power Pellets allows Pac-Man to eat the ghosts." + Environment.NewLine + "When you see the Hans ghost you lose 2 lives and 50 point.";
            lblCreators.Content = "Creators:";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
