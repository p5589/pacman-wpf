﻿namespace PacManGame
{
    public class _500DotsAchievement : Achievement
    {
        public _500DotsAchievement()
        {
            AchievementID = DataManager.GetAllAchievements()[1].AchievementID;
            Name = DataManager.GetAllAchievements()[1].Name;
            Description = DataManager.GetAllAchievements()[1].Description;
        }
        //Check 500 eaten dots
        public override void CheckIfConditionsAreMet(User user)
        {
            if (user.EatenDots == 500)
            {
                this.TriggerAchievement(user.UserID, this.AchievementID);
            }
        }
    }
}
