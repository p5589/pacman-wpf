﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace PacManGame
{
    public static class GameManager
    {
        public static DispatcherTimer timer = new DispatcherTimer();

        public static List<IGetsRendered> gameObjects;

        public static List<Enemy> enemies;

        public static Player player = new Player();

        public static Canvas ctx;

        public static List<int[,]> listMaps;

        public static _25DotsAchievement Achievement25dots = new _25DotsAchievement();
        public static _500DotsAchievement Achievement500dots = new _500DotsAchievement();
        public static _50PPAchievement Achievement50pp = new _50PPAchievement();
        public static _50GhostsAchievement Achievement50ghosts = new _50GhostsAchievement();

        public static int mapIndex = 0;

        public static User thisUser;

        public static DateTime AchievementPopUp;


        public static int[,] mapLevel1 = new int[20, 20]{
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
{1,2,2,2,2,2,2,4,2,2,2,2,2,2,2,2,2,2,3,1},
{1,2,1,2,1,2,1,2,1,2,1,1,1,1,1,1,1,1,2,1},
{1,3,1,2,1,2,1,1,1,2,2,2,6,1,2,2,2,1,2,1},
{1,2,1,2,1,2,2,2,1,1,1,1,2,1,2,1,2,1,2,1},
{1,2,1,2,1,2,1,2,2,2,2,1,2,1,2,1,2,1,2,1},
{1,2,1,2,1,1,1,1,1,1,2,1,2,1,5,1,2,1,2,1},
{1,2,1,2,2,2,2,2,2,1,3,1,2,1,2,1,2,1,2,1},
{1,2,1,1,1,1,1,1,8,1,2,1,2,2,2,1,2,1,2,1},
{1,2,2,2,2,2,2,1,1,1,1,1,1,6,2,1,2,1,2,1},
{1,5,1,1,1,1,2,1,2,2,2,2,1,1,2,1,2,2,5,1},
{1,2,2,2,2,1,2,1,2,2,1,2,2,1,7,1,1,1,2,1},
{1,2,1,2,1,1,2,2,3,1,1,1,2,1,2,2,2,1,2,1},
{1,2,1,2,2,1,1,2,1,1,2,1,2,1,2,1,2,2,6,1},
{1,2,1,2,1,1,2,2,2,1,2,1,2,1,1,1,1,1,2,1},
{1,2,1,2,2,2,2,1,2,2,2,1,2,2,2,2,2,2,2,1},
{1,2,1,1,1,1,1,1,1,2,1,1,1,2,1,2,1,2,2,1},
{1,2,2,2,2,2,2,2,2,2,2,7,1,1,1,1,1,1,2,1},
{1,2,1,2,1,6,1,5,1,2,1,2,2,2,2,3,2,2,2,1},
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
        };
        public static int[,] mapLevel2 = new int[20, 20] {
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
{1,3,2,0,0,2,2,2,2,3,0,2,0,0,2,0,2,5,3,1},
{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,2,1},
{1,2,1,5,1,0,1,1,1,0,2,0,2,1,2,0,2,1,2,1},
{1,0,1,0,1,0,0,2,1,1,1,1,0,1,0,1,2,1,2,1},
{1,2,1,2,1,0,1,0,2,0,2,1,2,1,4,1,0,1,2,1},
{1,0,1,0,1,1,1,1,1,1,0,1,2,1,0,1,2,1,0,1},
{1,2,1,2,0,2,0,2,2,1,2,1,2,1,2,1,0,1,2,1},
{1,5,1,1,1,1,1,1,7,1,0,1,0,0,0,1,2,1,0,1},
{1,0,2,0,2,0,0,1,1,1,1,1,1,0,2,1,0,1,2,1},
{1,3,1,1,1,1,2,1,5,3,0,0,1,1,0,1,2,0,3,1},
{1,2,0,2,0,1,0,1,0,0,1,0,2,1,2,1,1,1,0,1},
{1,2,1,0,1,1,0,2,0,1,1,1,0,1,0,2,0,1,2,1},
{1,0,1,2,0,1,1,0,1,1,6,1,2,1,6,1,0,0,0,1},
{1,2,1,0,1,1,0,2,2,1,0,1,0,1,1,1,1,1,2,1},
{1,0,1,5,2,0,2,1,0,2,0,1,2,0,2,0,2,0,2,1},
{1,2,1,1,1,1,1,1,1,0,1,1,1,0,1,0,1,0,2,1},
{1,0,2,0,2,0,2,0,2,0,2,0,1,1,1,1,1,1,0,1},
{1,3,1,2,1,0,1,3,1,2,1,2,2,5,0,2,0,0,3,1},
{ 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
        };

        public static int[,] mapLevel3 = new int[20, 20] {
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
{1,5,2,3,2,2,2,2,0,2,0,2,5,2,0,0,2,2,5,1},
{1,4,1,0,1,0,1,2,1,0,1,0,1,1,2,1,0,1,2,1},
{1,2,1,5,1,1,1,2,1,2,1,0,1,1,2,1,2,1,2,1},
{1,2,1,6,0,6,0,2,1,0,1,2,0,0,2,1,3,1,2,1},
{1,2,1,1,0,1,1,3,1,2,1,1,7,1,0,1,2,1,2,1},
{1,2,1,0,0,1,1,2,1,1,1,1,1,1,0,1,2,1,0,1},
{1,2,1,0,1,1,1,0,0,2,0,0,1,1,2,1,2,1,2,1},
{1,2,0,2,0,2,2,2,2,1,1,2,0,4,0,1,2,1,2,1},
{1,1,0,1,0,1,1,1,1,1,1,2,1,1,1,1,0,1,2,1},
{1,8,0,1,2,1,2,5,0,2,1,2,3,2,2,0,0,1,2,1},
{1,2,1,1,3,1,0,1,1,2,1,1,0,1,0,1,1,1,3,1},
{1,2,1,1,2,1,2,3,1,0,1,1,0,1,1,0,1,0,0,1},
{1,2,1,1,2,1,2,1,1,2,0,2,2,2,2,6,1,2,1,1},
{1,2,1,0,0,1,0,6,1,0,1,0,1,0,1,2,1,2,1,1},
{1,2,1,2,1,1,0,1,1,0,1,2,1,7,1,2,1,2,0,1},
{1,2,1,0,0,2,2,0,0,2,1,0,1,1,1,0,1,1,0,1},
{1,5,1,0,1,1,1,1,1,1,1,0,1,3,1,0,1,1,0,1},
{1,3,2,2,2,0,0,0,2,0,2,2,2,0,0,2,0,2,5,1},
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
        };

        public static int[,] csharpMap = new int[20, 20] {{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
{1,3,0,0,0,0,1,5,0,1,1,0,0,1,0,0,0,5,3,1},
{1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1},
{1,0,1,2,2,0,1,0,1,1,1,1,0,1,0,2,2,1,0,1},
{1,0,1,2,0,1,1,0,0,2,2,0,0,1,1,0,2,1,0,1},
{1,0,0,0,1,1,0,0,1,0,0,1,0,6,1,1,0,0,0,1},
{1,5,1,0,0,0,0,1,1,1,1,1,1,0,0,0,0,1,0,1},
{1,2,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,2,1},
{1,2,1,0,0,1,1,1,0,0,0,1,0,0,1,0,0,1,2,1},
{1,2,1,0,1,0,0,0,0,0,1,1,1,1,1,1,5,1,2,1},
{1,2,0,0,1,0,0,0,0,0,0,1,0,0,1,0,0,0,2,1},
{1,2,1,0,1,0,3,0,0,0,0,1,0,0,1,0,0,1,2,1},
{1,2,1,0,1,0,0,0,0,0,1,1,1,1,1,1,0,1,2,1},
{1,2,1,0,2,1,1,1,0,0,0,1,0,0,1,2,0,1,2,1},
{1,0,0,1,2,2,2,0,0,0,0,0,0,2,2,2,1,0,0,1},
{1,0,0,0,1,0,1,1,1,0,0,1,1,1,0,1,0,0,0,1},
{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,1},
{1,0,0,1,1,1,1,1,1,0,0,1,1,1,1,1,1,0,0,1},
{1,4,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,3,1},
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}};

        public static int[,] basicmap = new int[20, 20] {
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
{1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
{ 1,0,1,0,1,0,1,1,1,0,0,0,0,1,0,0,0,1,0,1},
{ 1,0,1,0,1,0,0,0,1,1,1,1,0,1,0,1,0,1,0,1},
{ 1,0,1,0,1,0,1,0,0,0,0,1,0,1,0,1,0,1,0,1},
{ 1,0,1,0,1,1,1,1,1,1,0,1,0,1,0,1,0,1,0,1},
{ 1,0,1,0,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,1},
{ 1,0,1,1,1,1,1,1,0,1,0,1,0,0,0,1,0,1,0,1},
{ 1,4,0,0,0,0,0,1,1,1,1,1,1,0,0,1,0,1,0,1},
{ 1,0,1,1,1,1,0,1,0,0,0,0,1,1,0,1,0,0,0,1},
{ 1,0,0,0,0,1,0,1,0,0,1,0,0,1,0,1,1,1,0,1},
{ 1,0,1,0,1,1,0,0,0,1,1,1,0,1,0,0,0,1,0,1},
{ 1,0,1,0,0,1,1,0,1,1,0,1,0,1,0,1,0,0,0,1},
{ 1,0,1,0,1,1,0,0,0,1,0,1,0,1,1,1,1,1,0,1},
{ 1,0,1,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1},
{ 1,0,1,1,1,1,1,1,1,0,1,1,1,0,1,0,1,0,0,1},
{ 1,5,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,1},
{ 1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,1},
{ 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
        };

        public static int[,] testMap1 = new int[20, 20]
        {
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
{1,3,0,0,5,0,0,5,0,3,0,0,0,5,0,0,0,5,3,1},
{ 1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
{ 1,0,1,5,1,0,1,1,1,0,0,0,0,1,0,0,0,1,0,1},
{ 1,0,1,0,1,0,0,0,1,1,1,1,0,1,0,1,0,1,0,1},
{ 1,0,1,0,1,0,1,0,0,0,0,1,0,1,4,1,0,1,0,1},
{ 1,0,1,0,1,1,1,1,1,1,0,1,0,1,6,1,0,1,0,1},
{ 1,0,1,0,0,0,0,0,0,1,0,1,0,1,6,1,0,1,0,1},
{ 1,5,1,1,1,1,1,1,7,1,0,1,0,0,6,1,0,1,0,1},
{ 1,0,0,0,0,0,0,1,1,1,1,1,1,0,2,1,0,1,0,1},
{ 1,3,1,1,1,1,0,1,5,3,0,0,1,1,0,1,0,0,3,1},
{ 1,0,0,0,0,1,0,1,0,0,1,0,0,1,0,1,1,1,0,1},
{ 1,0,1,0,1,1,0,0,0,1,1,1,0,1,0,0,0,1,0,1},
{ 1,0,1,0,0,1,1,0,1,1,6,1,0,1,6,1,0,0,0,1},
{ 1,0,1,0,1,1,0,0,0,1,0,1,0,1,1,1,1,1,0,1},
{ 1,0,1,5,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1},
{ 1,0,1,1,1,1,1,1,1,0,1,1,1,0,1,0,1,0,0,1},
{ 1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,1},
{ 1,3,1,0,1,0,1,3,1,0,1,0,0,5,0,0,0,0,3,1},
{ 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
        };

        public static int[,] testMap2 = new int[20, 20]
{
{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
{1,3,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,5,3,1},
{ 1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1},
{ 1,0,1,5,1,0,1,1,1,0,0,0,0,1,0,0,0,1,0,1},
{ 1,0,1,0,1,0,0,0,1,1,1,1,0,1,0,1,0,1,0,1},
{ 1,0,1,0,1,0,1,0,0,0,0,1,0,1,0,1,0,1,0,1},
{ 1,0,1,0,1,1,1,1,1,1,0,1,0,1,0,1,0,1,0,1},
{ 1,0,1,0,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,1},
{ 1,5,1,1,1,1,1,1,7,1,0,1,0,0,0,1,0,1,0,1},
{ 1,0,0,0,0,0,0,1,1,1,1,1,1,0,0,1,0,1,0,1},
{ 1,3,1,1,1,1,0,1,5,3,0,0,1,1,0,1,0,0,3,1},
{ 1,0,0,0,0,1,0,1,0,0,1,0,0,1,0,1,1,1,0,1},
{ 1,0,1,0,1,1,0,0,0,1,1,1,0,1,0,0,0,1,0,1},
{ 1,0,1,0,0,1,1,0,1,1,6,1,0,1,6,1,0,0,0,1},
{ 1,0,1,0,1,1,0,0,0,1,0,1,0,1,1,1,1,1,0,1},
{ 1,0,1,5,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1},
{ 1,0,1,1,1,1,1,1,1,0,1,1,1,0,1,0,1,0,0,1},
{ 1,4,6,6,6,6,0,0,0,0,2,0,1,1,1,1,1,1,0,1},
{ 1,3,1,0,1,0,1,3,1,0,1,0,0,5,0,0,0,0,3,1},
{ 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
};
        public static int[,] customMap = null;

        public static int[,] currentMap = testMap1;

        public static List<int[,]> maps;

        public static string[] colors = new string[] { "Red", "Blue", "Green", "Orange", "Pink", "Purple" };
        //2 = dot
        //3 = powerpellet => consumable ghosts
        //4 = pacman
        //5 = ghost
        //


        public static void Start(Canvas canvas, User thisuser, bool startOfNewGame = true)
        {
            if (startOfNewGame)
            {
                player = new Player();
                mapIndex = 0;
            }
            if (!startOfNewGame)
            {
                Sound.StartMusic();
            }
            int colorIndex = 0;

            maps = new List<int[,]>();
            if (customMap != null)
            {
                maps.Add(customMap);
            }
            maps.Add(mapLevel2);
            maps.Add(mapLevel3);
            currentMap = maps[mapIndex % maps.Count];
            int maxItemsToSwap = (int)mapIndex / maps.Count;
            canvas.Children.Clear();
            gameObjects = new List<IGetsRendered>();
            enemies = new List<Enemy>();
            ctx = canvas;
            Dot.coinConsumedEvent += D_coinConsumedEvent;
            ExtraLife.extraLifeConsumedEvent += E_extraLifeConsumedEvent;
            Fruit.fruitConsumedEvent += F_fruitConsumedEvent;
            PowerPellet.powerPelletConsumedEvent += P_powerPelletConsumedEvent;

            player.DieEvent += player_DieEvent;
            UI.UpdateHUD(player);
            thisUser = thisuser;

            //Spawn characters and items
            for (int i = 0; i < currentMap.GetLength(0); i++)
            {
                for (int j = 0; j < currentMap.GetLength(1); j++)
                {
                    switch (currentMap[i, j])
                    {
                        case 2:
                            gameObjects.Add(new Dot(new Point(MapSpecification.TileSize * j, MapSpecification.TileSize * i)));
                            break;
                        case 3:
                            gameObjects.Add(new PowerPellet(new Point(MapSpecification.TileSize * j, MapSpecification.TileSize * i)));
                            break;
                        case 4:
                            player.OriginalLocation = new Point(MapSpecification.TileSize * j, MapSpecification.TileSize * i);
                            break;
                        case 5:
                            Ghost ghost1 = new Ghost(colors[colorIndex % colors.Length]);
                            colorIndex++;
                            ghost1.OriginalLocation = new Point(MapSpecification.TileSize * j, MapSpecification.TileSize * i);
                            ghost1.Spawn();
                            enemies.Add(ghost1);
                            break;
                        case 6:
                            gameObjects.Add(new Fruit(new Point(MapSpecification.TileSize * j, MapSpecification.TileSize * i)));
                            break;
                        case 7:
                            gameObjects.Add(new ExtraLife(new Point(MapSpecification.TileSize * j, MapSpecification.TileSize * i)));
                            break;
                        case 8:
                            HansGhost hansGhost = new HansGhost();
                            hansGhost.OriginalLocation = new Point(MapSpecification.TileSize * j, MapSpecification.TileSize * i);
                            hansGhost.Spawn();
                            enemies.Add(hansGhost);
                            break;
                        default:
                            break;
                    }
                }
            }

            int swappedGhosts = 0;
            for (int i = 0; i < enemies.Count; i++)
            {
                if (swappedGhosts >= maxItemsToSwap)
                {
                    break;
                }
                if (enemies[i] is Ghost ghost)
                {
                    HansGhost tempHans = new HansGhost();

                    tempHans.OriginalLocation = ghost.Location;
                    tempHans.Spawn();
                    enemies[i] = tempHans;
                    swappedGhosts++;
                }
            }
            //Add enemies after items to ensure correct draw order
            foreach (var enemy in enemies)
            {
                gameObjects.Add(enemy);
            }
            //Swap pellets for dots and ghosts for Hans
            int removedPellets = 0;
            for (int i = 0; i < gameObjects.Count; i++)
            {
                if (removedPellets >= maxItemsToSwap)
                {
                    break;
                }
                if (gameObjects[i] is PowerPellet pellet)
                {
                    gameObjects[i] = new Dot(pellet.Location);
                    removedPellets++;
                }
            }


            player.Direction = new Point(1, 0);
            player.Spawn();
            gameObjects.Add(player);
            timer.Interval = TimeSpan.FromMilliseconds(30);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private static void P_powerPelletConsumedEvent()
        {
            Sound.PlayEatPowerPellet();
            UI.UpdateHUD(player);
            thisUser.EatenPowerPellets++;
            Achievement50pp.CheckIfConditionsAreMet(thisUser);
        }

        private static void F_fruitConsumedEvent()
        {
            Sound.PlayEatFruit();
            UI.UpdateHUD(player);
        }

        private static void E_extraLifeConsumedEvent()
        {
            Sound.PlayExtraPac();
            UI.UpdateHUD(player);
        }

        private static void D_coinConsumedEvent()
        {
            Sound.PlayChomp();
            UI.UpdateHUD(player);
            thisUser.EatenDots++;
            Achievement25dots.CheckIfConditionsAreMet(thisUser);
            Achievement500dots.CheckIfConditionsAreMet(thisUser);
        }

        public static void RenderFrame()
        {
            ctx.Children.Clear();
            MapDrawer.Generate(currentMap);
            foreach (var gameObject in gameObjects)
            {
                if (gameObject is IStillSprite still)
                {
                    if (still is IConsumable consumable && consumable.IsConsumed)
                    {
                        continue;
                    }

                    BitmapImage image = new BitmapImage(new Uri(still.SpritePath, UriKind.Relative));
                    ImageBrush myImageBrush = new ImageBrush(image);
                    Canvas myCanvas = new Canvas();
                    myCanvas.Width = MapSpecification.TileSize;
                    myCanvas.Height = MapSpecification.TileSize;
                    myCanvas.Background = myImageBrush;
                    Canvas.SetTop(myCanvas, still.Location.Y);
                    Canvas.SetLeft(myCanvas, still.Location.X);
                    ctx.Children.Add(myCanvas);
                }

                if (gameObject is IAnimatable p)
                {
                    Animation anim = p.Animation;
                    BitmapImage image = anim.Frames[p.CurrentFrame];
                    ImageBrush myImageBrush = new ImageBrush(image);
                    Canvas myCanvas = new Canvas();
                    myCanvas.Width = MapSpecification.TileSize;
                    myCanvas.Height = MapSpecification.TileSize;
                    myCanvas.Background = myImageBrush;
                    Canvas.SetTop(myCanvas, p.Location.Y);
                    Canvas.SetLeft(myCanvas, p.Location.X);
                    ctx.Children.Add(myCanvas);
                    p.CurrentFrame++;
                    if (p is Player pl)
                    {
                        if (pl.Lives <= 0 && p.CurrentFrame == anim.Frames.Count)
                        {
                            StopGame();
                        }
                    }
                    if (p.CurrentFrame >= anim.Frames.Count)
                    {
                        p.CurrentFrame = 0;
                    }
                }
            }
        }

        public static void HandleDataBase()
        {
            if (thisUser != null)
            {
                DataManager.AddHighscore(thisUser.UserID, player.Score);

                if (DataManager.ModifyUser(thisUser) < 1)
                {
                    MessageBox.Show("User could not be modified in the database.", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        public static void StopGame()
        {
            HandleDataBase();
            timer.Stop();
            UnsubscribeFromEvents();

            UI.ShowGameOver();
        }
        public static void CompleteLevelGame()
        {
            timer.Stop();
            UnsubscribeFromEvents();
            player.StopPowerPelletPower();
            mapIndex++;
        }

        public static void UnsubscribeFromEvents()
        {
            timer.Tick -= Timer_Tick;
            Dot.coinConsumedEvent -= D_coinConsumedEvent;
            ExtraLife.extraLifeConsumedEvent -= E_extraLifeConsumedEvent;
            Fruit.fruitConsumedEvent -= F_fruitConsumedEvent;
            PowerPellet.powerPelletConsumedEvent -= P_powerPelletConsumedEvent;
            player.DieEvent -= player_DieEvent;
        }


        private static void Timer_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now > AchievementPopUp.AddSeconds(4))
            {
                HideAllPopups();
            }
            if (AreAllDotsConsumed())
            {
                CompleteLevelGame();
                Start(ctx, thisUser, false);
            }
            CheckCollisions();
            player.UpdatePlayer();
            UpdateEnemies();
            RenderFrame();
        }

        private static void UpdateEnemies()
        {
            foreach (var enemy in enemies)
            {
                enemy.UpdateCharacter();
            }
        }

        public static Point LocationToTileLocation(Point p)
        {
            return new Point((int)(p.X / MapSpecification.TileSize), (int)(p.Y / MapSpecification.TileSize));
        }
        private static bool AreAllDotsConsumed()
        {
            foreach (var gameObject in gameObjects)
            {
                if (gameObject is Dot dot && !dot.IsConsumed)
                {
                    return false;
                }
            }
            return true;
        }
        //TODO better detection
        private static void CheckCollisions()
        {
            var playerTile = LocationToTileLocation(player.Location);
            foreach (var gameObject in gameObjects)
            {
                if (gameObject is Enemy enem)
                {
                    if (DoCollide(player.Location, enem.Location) && player.Lives > 0)
                    {
                        if ((DateTime.Now - player.StartTimeCanEatGhosts).TotalSeconds > PowerPellet.duration)
                        {
                            enem.DoDamage(player);

                            if (player.Lives == 0)
                            {
                                player.Speed = 0;
                            }
                            else
                            {
                                player.Spawn();
                            }
                            foreach (var enemy in enemies)
                            {
                                enemy.Spawn();
                            }
                        }
                        else
                        {
                            player.DoDamage(enem);
                            player.Score += enem.value;
                            thisUser.EatenGhosts++;
                            Achievement50ghosts.CheckIfConditionsAreMet(thisUser);
                        }
                        UI.UpdateHUD(player);
                    }
                }

                else if (gameObject is Dot dot)
                {
                    if (dot.IsConsumed == false)
                    {
                        //if (LocationToTileLocation(dot.Location) == playerTile)
                        if (DoCollide(player.Location, dot.Location))
                        {
                            player.Consume(dot);
                        }
                    }
                }
                else if (gameObject is Fruit fruit)
                {
                    if (fruit.IsConsumed == false)
                    {
                        if (DoCollide(player.Location, fruit.Location))
                        {
                            player.Consume(fruit);
                        }
                    }
                }
                else if (gameObject is PowerPellet powerPellet)
                {
                    if (powerPellet.IsConsumed == false)
                    {
                        if (DoCollide(player.Location, powerPellet.Location))
                        {
                            player.Consume(powerPellet);
                        }
                    }
                }
                else if (gameObject is ExtraLife extraLife)
                {
                    if (extraLife.IsConsumed == false)
                    {
                        if (DoCollide(player.Location, extraLife.Location))
                        {
                            player.Consume(extraLife);
                        }
                    }
                }
            }
        }

        public static bool DoCollide(Point p1, Point p2)
        {
            double radiusAdjustingMultiplier = 0.8;
            return Math.Pow((p2.X - p1.X), 2) + Math.Pow((p2.Y - p1.Y), 2) < Math.Pow(MapSpecification.TileSize, 2) * radiusAdjustingMultiplier;
        }

        private static void player_DieEvent(string a)
        {
            Sound.PlayDeath();
        }

        private static void HideAllPopups()
        {
            MainWindow Form = Application.Current.Windows[0] as MainWindow;
            Form.popAchievement1.IsOpen = false;
        }
    }
}
