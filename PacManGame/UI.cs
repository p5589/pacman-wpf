﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace PacManGame
{
    static class UI
    {
        public static MainWindow window { get; set; }

        //Call at Game start + Score/Life Change event -- not every tick

        public static void UpdateHUD(Player thisplayer)
        {
            window.lblScore.Content = thisplayer.Score.ToString();
            if (Convert.ToInt32(window.lblHighScore.Content) < thisplayer.Score)
            {
                window.lblScore.Foreground = new SolidColorBrush(Colors.Yellow);
                window.lblHighScore.Content = thisplayer.Score;
                window.lblHighScore.Foreground = new SolidColorBrush(Colors.Red);
                //Call HighScoreBrokenevent/method/sounds
            }
            window.lblLife.Content = thisplayer.Lives.ToString();
        }

        //Calls DB, only at game start
        public static void GetUserHighScore(User thisuser)
        {
            Highscore thisHighScore = new Highscore();
            window.lblHighScore.Content = thisHighScore.UserTopSCoreDB(thisuser).ToString();
        }

        //After Game Over Screen
        public static void ShowHighscores(User thisuser)
        {
            window.stkGameOver.Visibility = System.Windows.Visibility.Hidden;
            Highscore thisHighScore = new Highscore();
            List<Highscore> listHighScores = thisHighScore.TopHighscores;
            if (listHighScores.Count >= 15)
            {
                listHighScores.RemoveRange(10, 5);
            }
            foreach (Highscore x in listHighScores)
            {
                if (x.UserName == thisuser.Name)
                {
                    x.MyColour = new SolidColorBrush(Colors.Yellow);
                }
                else
                {
                    x.MyColour = new SolidColorBrush(Colors.Red);
                }
            }
            window.lbDisplayTopScore.ItemsSource = listHighScores;
            window.canvasPacman.Opacity = 0.2;
            window.stkLifes.Opacity = 0.2;
            window.stkScore.Opacity = 0.2;
            window.stkHighscore.Opacity = 0.2;
            window.stkTop15HighScores.Visibility = System.Windows.Visibility.Visible;
            window.lblScore.Foreground = new SolidColorBrush(Colors.White);
            window.lblHighScore.Foreground = new SolidColorBrush(Colors.White);
        }

        //Calls Game Over Screen after DeathAnimationRender, or to be called on player Death Event
        public static void ShowGameOver()
        {
            window.canvasPacman.Opacity = 0.2;
            window.stkLifes.Opacity = 0.2;
            window.stkScore.Opacity = 0.2;
            window.stkHighscore.Opacity = 0.2;
            window.stkGameOver.Visibility = System.Windows.Visibility.Visible;
            DispatcherTimer thisTimer;
            TimeSpan thisSpan = new TimeSpan();
            thisSpan = TimeSpan.FromSeconds(3);
            thisTimer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                if (thisSpan == TimeSpan.Zero)
                {
                    string naam = window.lblUser.Content.ToString();
                    naam = naam.Substring(6);
                    naam = naam.Remove((naam.Length - 1), 1);
                    User thisUser = new User();
                    thisUser.Name = naam;
                    ShowHighscores(thisUser);
                }
                thisSpan = thisSpan.Add(TimeSpan.FromSeconds(-1));
            }, Application.Current.Dispatcher);
            thisTimer.Start();
        }
    }
}
