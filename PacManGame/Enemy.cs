﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;

namespace PacManGame
{
    public abstract class Enemy : Character
    {
        protected bool EdibleByPlayer { get; set; } = false;

        protected int amountOfCoolDownSeconds = 1;

        private int chanceToContinue = 80;

        public int value = 50;
        public DateTime TimeLastTurn { get; set; }

        public void UpdateCharacter()
        {
            Point newDirection = Direction;

            List<Point> validDirections = new List<Point>();
            bool canGoForward = false;

            //Is continueing forward valid
            if (IsMoveInDirectionValid(this, this.Direction))
            {
                validDirections.Add(this.Direction);
                canGoForward = true;
            }

            //Check if perpundicullar directions are valid
            Point perpundicullar1 = new Point(Direction.Y, Direction.X);
            Point perpundicullar2 = new Point(-Direction.Y, -Direction.X);
            if (IsMoveInDirectionValid(this, perpundicullar1))
            {
                validDirections.Add(perpundicullar1);
            }

            if (IsMoveInDirectionValid(this, perpundicullar2))
            {
                validDirections.Add(perpundicullar2);
            }
            // Weigh options
            if (validDirections.Count == 0)
            {
                //Turn around
                Direction = new Point(-Direction.X, -Direction.Y);
            }
            else if (canGoForward)
            {
                if (rng.Next(0, 100) <= chanceToContinue || (DateTime.Now - TimeLastTurn).TotalSeconds < amountOfCoolDownSeconds)
                {
                    //Move forward
                    Direction = validDirections[0];
                }
                else
                {
                    //Take turn
                    if (validDirections.Count > 1)
                    {
                        Direction = validDirections[rng.Next(1, validDirections.Count)];
                        Point closestTile = ClosestTile(Location);
                        Location = new Point(closestTile.X * MapSpecification.TileSize, closestTile.Y * MapSpecification.TileSize);
                        TimeLastTurn = DateTime.Now;
                    }
                }
            }
            else
            {
                //On T-interection take random turn
                Direction = validDirections[rng.Next(validDirections.Count)];
            }


            Move();
        }
        protected bool IsPacmanInLineOfSight()
        {

            return false;
        }
    }
}
