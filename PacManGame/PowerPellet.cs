﻿using System.Windows;

namespace PacManGame
{
    class PowerPellet : Item, IConsumable, IStillSprite
    {
        public const int duration = 10;
        public readonly int value = 50;
        public PowerPellet(Point location)
        {
            Location = location;
        }
        public string ConsumeSfxPath { get; set; }
        public bool IsConsumed { get; set; }
        public string SpritePath { get; set; } = "Images/PowerPellet.png";

        public void GetConsumed()
        {
            if (!IsConsumed)
            {
                IsConsumed = true;
                powerPelletConsumedEvent?.Invoke();
            }
        }
        public delegate void PowerPelletConsumed();
        public static event PowerPelletConsumed powerPelletConsumedEvent;
    }
}
