﻿using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace PacManGame
{
    public partial class User
    {

        private BitmapImage _profilePic;

        public BitmapImage ProfilePic
        {
            get { return _profilePic; }
            set { _profilePic = value; }
        }

        //Database
        public List<Achievement> Achievements
        {
            get
            { return DataManager.GetUserGainedAchievements(this.UserID); }
        }
    }
}
