﻿namespace PacManGame
{
    public class Ghost : Enemy, IAnimatable
    {
        private int _currentFrame = 0;
        private string _ghostColour;


        public Ghost(string colour)
        {
            Direction = new System.Windows.Point(1, 0);
            GhostColour = colour;
        }


        public string GhostColour
        {
            get { return _ghostColour; }
            set { _ghostColour = value; }
        }
        public int CurrentFrame
        {
            get { return _currentFrame; }
            set { _currentFrame = value; }
        }

        public override void TakeDamage(int amount)
        {
            base.TakeDamage(amount);
            Spawn();
        }

        public Animation Animation
        {
            get
            {
                Animation thisAnimation = new Animation();
                if (Direction.X == 1)
                {
                    thisAnimation = thisAnimation.Dictionary[$"{GhostColour}Right"];
                }
                else if (Direction.X == -1)
                {
                    thisAnimation = thisAnimation.Dictionary[$"{GhostColour}Left"];
                }
                else if (Direction.Y == -1)
                {
                    thisAnimation = thisAnimation.Dictionary[$"{GhostColour}Up"];
                }
                else if (Direction.Y == 1)
                {
                    thisAnimation = thisAnimation.Dictionary[$"{GhostColour}Down"];
                }
                return thisAnimation;
            }
        }
    }
}
