﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PacManGame
{
    public interface IGetsRendered
    {
        Point Location { get; set; }
    }
}
