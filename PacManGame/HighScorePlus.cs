﻿using System.Collections.Generic;
using System.Windows.Media;

namespace PacManGame
{
    public partial class Highscore
    {
        private List<Highscore> _topHighScores;
        public string UserName
        {
            get { return DataManager.GetOneUser(this.UserID).Name; }
        }
        public int Rank { get; set; }
        public string Color { get; set; }
        public string OnlyDate
        {
            get
            {
                return this.Date.ToString("dd-MM-yyyy");
            }
        }
        public int UserTopSCoreDB(User thisUser)
        {
            Highscore thishighscore = DataManager.GetTopHighscoreUser(thisUser.UserID);
            return thishighscore.Value;
        }
        public SolidColorBrush MyColour { get; set; }

        public List<Highscore> TopHighscores
        {
            get
            {
                _topHighScores = DataManager.GetTop15Highscores();
                return _topHighScores;
            }
            set { _topHighScores = value; }
        }

    }
}
