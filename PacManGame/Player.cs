﻿using System;
using System.Windows;
using System.Windows.Input;

namespace PacManGame
{
    public class Player : Character, IAnimatable
    {
        private int _currentFrame;
        public string DieSfxPath { get; set; }
        public Player()
        {
            StartTimeCanEatGhosts = DateTime.Today.AddDays(-1); //Arbitrary date longer than 10 seconds ago
        }
        public bool canEatGhosts
        {
            get { return (DateTime.Now - StartTimeCanEatGhosts).TotalSeconds < PowerPellet.duration; }

        }
        public int CurrentFrame
        {
            get { return _currentFrame; }
            set { _currentFrame = value; }
        }
        public int Dots { get; set; }
        public int Score { get; set; }

        public int ExtraLives { get; set; }

        public int Fruits { get; set; }

        public int PowerPellets { get; set; }

        public Animation Animation
        {
            get
            {
                Animation thisAnimation = new Animation();
                if (Lives <= 0)
                {
                    thisAnimation = thisAnimation.Dictionary["PacManDeath"];
                }
                else if (Direction.X == 1)
                {
                    thisAnimation = thisAnimation.Dictionary["PacManRight"];
                }
                else if (Direction.X == -1)
                {
                    thisAnimation = thisAnimation.Dictionary["PacManLeft"];
                }
                else if (Direction.Y == -1)
                {
                    thisAnimation = thisAnimation.Dictionary["PacManUp"];
                }
                else if (Direction.Y == 1)
                {
                    thisAnimation = thisAnimation.Dictionary["PacManDown"];
                }
                return thisAnimation;
            }
        }


        public DateTime StartTimeCanEatGhosts { get; private set; }

        public void Consume(IConsumable item)
        {
            IConsumable itemToConsume = item as IConsumable;
            if (itemToConsume is Dot)
            {
                Dot dot = itemToConsume as Dot;
                Dots += 1;
                Score += dot.value;
            }
            else
            if (itemToConsume is PowerPellet pellet)
            {
                StartTimeCanEatGhosts = DateTime.Now;
                PowerPellets++;
                Score += pellet.value;
            }
            else
            if (itemToConsume is Fruit fruit)
            {
                Score += fruit.value;
                Fruits++;
            }
            else if (itemToConsume is ExtraLife)
            {
                Lives++;
                ExtraLives++;
            }
            itemToConsume.GetConsumed();
        }

        public void StopPowerPelletPower()
        {
            StartTimeCanEatGhosts = DateTime.Today.AddDays(-1); //Arbitrary date longer than 10 seconds ago
        }
        public void UpdatePlayer()
        {
            Point newDirection = Direction;

            bool cornerTaken = false;
            if (Keyboard.IsKeyDown(Key.Up))
            {
                newDirection = new Point(0, -1);
                cornerTaken = ArePerpundicullar(newDirection, Direction);
            }
            else if (Keyboard.IsKeyDown(Key.Down))
            {
                newDirection = new Point(0, 1);
                cornerTaken = ArePerpundicullar(newDirection, Direction);
            }
            else if (Keyboard.IsKeyDown(Key.Right))
            {
                newDirection = new Point(1, 0);
                cornerTaken = ArePerpundicullar(newDirection, Direction);
            }
            else if (Keyboard.IsKeyDown(Key.Left))
            {
                newDirection = new Point(-1, 0);
                cornerTaken = ArePerpundicullar(newDirection, Direction);
            }

            Direction = newDirection;
            if (IsMoveInDirectionValid(this, this.Direction))
            {
                if (cornerTaken)
                {
                    Point closestTile = ClosestTile(Location);
                    //  if (GameManager.currentMap[(int)(closestTile.Y + this.Direction.Y), (int)(closestTile.X + this.Direction.X)] != 1)
                    // {
                    Location = new Point(closestTile.X * MapSpecification.TileSize, closestTile.Y * MapSpecification.TileSize);
                    // }
                }
                Move();
            }


        }
        public Point NextTileIncurrentDirection(Point position, Point direction)
        {

            return new Point(0, 0);
        }
    }
}
