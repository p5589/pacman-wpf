﻿using System;
using System.Media;
using System.Windows.Media;

namespace PacManGame
{
    static class Sound
    {
        public static MediaPlayer musicPlayer = new MediaPlayer();

        public static SoundPlayer sfxPlayer = new SoundPlayer();

        public static bool isMuted = false;

        public static void PlaySfx(string path)
        {
            if (!isMuted)
            {
                sfxPlayer.SoundLocation = path;
                sfxPlayer.Play();
            }

        }

        public static void StartMusic()
        {

            musicPlayer.ScrubbingEnabled = false;
            musicPlayer.Open(new Uri(@"Sounds\pacman_beginning.wav", UriKind.Relative));
            musicPlayer.Play();
            musicPlayer.MediaEnded += MusicThemeWrapper;

        }



        public static void StopMusic()
        {
            musicPlayer.ScrubbingEnabled = true;
            musicPlayer.Stop();
        }

        public static void Unmute()
        {
            musicPlayer.Volume = 0.5;
            isMuted = false;
        }

        public static void Mute()
        {
            musicPlayer.Volume = 0;
            isMuted = true;
        }


        private static void MusicThemeWrapper(object sender, EventArgs e)
        {
            PlayThemeSong();
        }
        public static void PlayThemeSong()
        {

            musicPlayer.Open(new Uri(@"Sounds\BackGroundSong.wav", UriKind.Relative));
            musicPlayer.Play();
            musicPlayer.MediaEnded += MusicThemeWrapper;

        }

        public static void PlayDeath()
        {
            PlaySfx(@"Sounds\pacman_death.wav");

        }
        public static void PlayChomp()
        {
            PlaySfx(@"Sounds\pacman_chomp.wav");

        }
        public static void PlayEatFruit()
        {
            PlaySfx(@"Sounds\pacman_eatfruit.wav");

        }
        public static void PlayEatGhost()
        {
            PlaySfx(@"Sounds\pacman_eatghost.wav");

        }
        public static void PlayExtraPac()
        {
            PlaySfx(@"Sounds\pacman_extrapac.wav");

        }
        public static void PlayEatPowerPellet()
        {
            //PlaySfx(@"Sounds\pacman_eatingPowerPellet.wav");
            musicPlayer.Open(new Uri(@"Sounds\pacman_eatingPowerPellet.wav", UriKind.Relative));
            musicPlayer.Play();
        }
    }
}
