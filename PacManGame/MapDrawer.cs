﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace PacManGame
{
    internal static class MapDrawer
    {
        private static List<BitmapImage> wallCross = new List<BitmapImage>()
        { new BitmapImage(new Uri("Images\\WallCross.png", UriKind.Relative)),
          new BitmapImage(new Uri("Images\\WallCross1.png", UriKind.Relative))
        };
        private static List<BitmapImage> wallT = new List<BitmapImage>()
        { new BitmapImage(new Uri("Images\\WallT.png", UriKind.Relative)),
          new BitmapImage(new Uri("Images\\WallT1.png", UriKind.Relative))
        };
        private static List<BitmapImage> wallCorner = new List<BitmapImage>()
        { new BitmapImage(new Uri("Images\\WallCorner.png", UriKind.Relative)),
          new BitmapImage(new Uri("Images\\WallCorner1.png", UriKind.Relative))
        };
        private static List<BitmapImage> wallStraight = new List<BitmapImage>()
        { new BitmapImage(new Uri("Images\\WallStraight.png", UriKind.Relative)),
          new BitmapImage(new Uri("Images\\WallStraight1.png", UriKind.Relative))
        };
        private static List<BitmapImage> wallEnd = new List<BitmapImage>()
        { new BitmapImage(new Uri("Images\\WallEnd.png", UriKind.Relative)),
          new BitmapImage(new Uri("Images\\WallEnd1.png", UriKind.Relative))
        };
        private static List<BitmapImage> wallBlock = new List<BitmapImage>()
        { new BitmapImage(new Uri("Images\\WallBlock.png", UriKind.Relative)),
          new BitmapImage(new Uri("Images\\WallBlock1.png", UriKind.Relative))
        };
        private static BitmapImage currentWallPiece;
        private static int rotation;
        private static int frameCount = 0;

        public static int[,] TestMap { get; set; }
        public static Canvas canvas { get; set; }
        public static void Generate(int[,] testMap)
        {
            int frame = 0;
            int frameDuration = 4;
            if (GameManager.player.canEatGhosts)
            {
                frame = (int)(frameCount / frameDuration) % 2;
                frameCount++;
            }
            else
            {
                frameCount = 0;
            }
            TestMap = testMap;
            for (int i = 0; i < testMap.GetLength(0); i++)
            {
                for (int j = 0; j < testMap.GetLength(1); j++)
                {
                    if (testMap[i, j] == 1)
                    {
                        System.Drawing.Point position = new System.Drawing.Point(i * MapSpecification.TileSize, j * MapSpecification.TileSize);
                        currentWallPiece = wallStraight[frame];
                        //Figure out piece and rotation
                        rotation = 0;
                        if (IsThereAWallUp(j, i) && IsThereAWallRight(j, i) && IsThereAWallLeft(j, i) && IsThereAWallDown(j, i))
                        {
                            currentWallPiece = wallCross[frame];
                        }
                        else if (IsThereAWallUp(j, i) && IsThereAWallRight(j, i) && IsThereAWallLeft(j, i))
                        {
                            currentWallPiece = wallT[frame];
                            rotation = 180;
                        }
                        else if (IsThereAWallUp(j, i) && IsThereAWallRight(j, i) && IsThereAWallDown(j, i))
                        {
                            currentWallPiece = wallT[frame];
                            rotation = 270;
                        }
                        else if (IsThereAWallUp(j, i) && IsThereAWallLeft(j, i) && IsThereAWallDown(j, i))
                        {
                            currentWallPiece = wallT[frame];
                            rotation = 90;
                        }
                        else if (IsThereAWallRight(j, i) && IsThereAWallLeft(j, i) && IsThereAWallDown(j, i))
                        {
                            currentWallPiece = wallT[frame];
                            rotation = 0;
                        }
                        else if (IsThereAWallUp(j, i) && IsThereAWallRight(j, i))
                        {
                            currentWallPiece = wallCorner[frame];
                            rotation = 270;
                        }
                        else if (IsThereAWallDown(j, i) && IsThereAWallRight(j, i))
                        {
                            currentWallPiece = wallCorner[frame];
                            rotation = 0;
                        }
                        else if (IsThereAWallUp(j, i) && IsThereAWallLeft(j, i))
                        {
                            currentWallPiece = wallCorner[frame];
                            rotation = 180;
                        }
                        else if (IsThereAWallDown(j, i) && IsThereAWallLeft(j, i))
                        {
                            currentWallPiece = wallCorner[frame];
                            rotation = 90;
                        }
                        else if (IsThereAWallRight(j, i) && IsThereAWallLeft(j, i))
                        {
                            currentWallPiece = wallStraight[frame];
                            rotation = 0;
                        }
                        else if (IsThereAWallUp(j, i) && IsThereAWallDown(j, i))
                        {
                            currentWallPiece = wallStraight[frame];
                            rotation = 90;
                        }
                        else if (IsThereAWallUp(j, i) && IsThereAWallDown(j, i))
                        {
                            currentWallPiece = wallStraight[frame];
                            rotation = 90;
                        }
                        else if (IsThereAWallRight(j, i) && IsThereAWallLeft(j, i))
                        {
                            currentWallPiece = wallStraight[frame];
                            rotation = 0;
                        }
                        else if (IsThereAWallRight(j, i))
                        {
                            currentWallPiece = wallEnd[frame];
                            rotation = 180;
                        }
                        else if (IsThereAWallLeft(j, i))
                        {
                            currentWallPiece = wallEnd[frame];
                            rotation = 0;
                        }
                        else if (IsThereAWallDown(j, i))
                        {
                            currentWallPiece = wallEnd[frame];
                            rotation = 270;
                        }
                        else if (IsThereAWallUp(j, i))
                        {
                            currentWallPiece = wallEnd[frame];
                            rotation = 90;
                        }
                        else
                        {
                            currentWallPiece = wallBlock[frame];
                        }
                        drawElement(position);
                    }
                }
            }
        }

        private static void drawElement(System.Drawing.Point p)
        {
            ImageBrush myImageBrush = new ImageBrush(currentWallPiece);
            Canvas myCanvas = new Canvas();
            myCanvas.Width = MapSpecification.TileSize;
            myCanvas.Height = MapSpecification.TileSize;
            myCanvas.Background = myImageBrush;

            RotateTransform rotateTransform2 = new RotateTransform(rotation);
            rotateTransform2.CenterX = myCanvas.Width / 2;
            rotateTransform2.CenterY = myCanvas.Height / 2;
            myCanvas.RenderTransform = rotateTransform2;

            Canvas.SetTop(myCanvas, p.X);
            Canvas.SetLeft(myCanvas, p.Y);
            canvas.Children.Add(myCanvas);
        }
        private static bool IsThereAWallUp(int x, int y)
        {
            if (y == 0)
            {
                return false;
            }
            if (TestMap[y - 1, x] == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool IsThereAWallDown(int x, int y)
        {
            if (y == TestMap.GetLength(0) - 1)
            {
                return false;
            }
            if (TestMap[y + 1, x] == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private static bool IsThereAWallRight(int x, int y)
        {
            if (x == TestMap.GetLength(1) - 1)
            {
                return false;
            }
            if (TestMap[y, x + 1] == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private static bool IsThereAWallLeft(int x, int y)
        {
            if (x == 0)
            {
                return false;
            }
            if (TestMap[y, x - 1] == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}


