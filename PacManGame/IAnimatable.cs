﻿namespace PacManGame
{
    public interface IAnimatable : IGetsRendered
    {
        int CurrentFrame { get; set; }
        Animation Animation { get; }
    }
}
