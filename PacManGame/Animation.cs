﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace PacManGame
{
    public class Animation
    {
        private List<BitmapImage> _frames;
        public bool _repeats = false;
        public static Dictionary<string, Animation> _dictionary;


        public Animation()
        {
            Frames = new List<BitmapImage>();
        }

        public Dictionary<string, Animation> Dictionary
        {
            get
            {
                _dictionary = GetDictionary();
                return _dictionary;
            }
        }
        public List<BitmapImage> Frames
        {
            get { return _frames; }
            set { _frames = value; }
        }
        public bool Repeats
        {
            get { return Repeats; }
            set { Repeats = value; }
        }


        //Creates Dictionary For Animation Interface with Name and AnimationSet
        public Dictionary<string, Animation> GetDictionary()
        {
            List<Animation> thislist = GetAnimations();
            Dictionary<string, Animation> thisDictionary = new Dictionary<string, Animation>();

            string name = "BlueUp";
            thisDictionary.Add(name, thislist[0]);
            name = "BlueRight";
            thisDictionary.Add(name, thislist[1]);
            name = "BlueDown";
            thisDictionary.Add(name, thislist[2]);
            name = "BlueLeft";
            thisDictionary.Add(name, thislist[3]);

            name = "RedUp";
            thisDictionary.Add(name, thislist[4]);
            name = "RedRight";
            thisDictionary.Add(name, thislist[5]);
            name = "RedDown";
            thisDictionary.Add(name, thislist[6]);
            name = "RedLeft";
            thisDictionary.Add(name, thislist[7]);

            name = "PurpleUp";
            thisDictionary.Add(name, thislist[8]);
            name = "PurpleRight";
            thisDictionary.Add(name, thislist[9]);
            name = "PurpleDown";
            thisDictionary.Add(name, thislist[10]);
            name = "PurpleLeft";
            thisDictionary.Add(name, thislist[11]);

            name = "GreenUp";
            thisDictionary.Add(name, thislist[12]);
            name = "GreenRight";
            thisDictionary.Add(name, thislist[13]);
            name = "GreenDown";
            thisDictionary.Add(name, thislist[14]);
            name = "GreenLeft";
            thisDictionary.Add(name, thislist[15]);

            name = "OrangeUp";
            thisDictionary.Add(name, thislist[16]);
            name = "OrangeRight";
            thisDictionary.Add(name, thislist[17]);
            name = "OrangeDown";
            thisDictionary.Add(name, thislist[18]);
            name = "OrangeLeft";
            thisDictionary.Add(name, thislist[19]);

            name = "PinkUp";
            thisDictionary.Add(name, thislist[20]);
            name = "PinkRight";
            thisDictionary.Add(name, thislist[21]);
            name = "PinkDown";
            thisDictionary.Add(name, thislist[22]);
            name = "PinkLeft";
            thisDictionary.Add(name, thislist[23]);

            name = "PacManUp"; ;
            thisDictionary.Add(name, thislist[24]);
            name = "PacManRight";
            thisDictionary.Add(name, thislist[25]);
            name = "PacManDown";
            thisDictionary.Add(name, thislist[26]);
            name = "PacManLeft";
            thisDictionary.Add(name, thislist[27]);

            // Death

            name = "PacManDeath"; ;
            thisDictionary.Add(name, thislist[28]);

            //Ghost Hans
            name = "HansGhost";
            thisDictionary.Add(name, thislist[29]);

            return thisDictionary;
        }

        //Creates and Gets List of All animations for use for Dictionary
        public List<Animation> GetAnimations()
        {
            List<Animation> thislist = new List<Animation>();

            //Move Animations

            string parameter = "Blue";
            thislist = GetAnimationsPerColour(parameter, thislist);
            parameter = "Red";
            thislist = GetAnimationsPerColour(parameter, thislist);
            parameter = "Purple";
            thislist = GetAnimationsPerColour(parameter, thislist);
            parameter = "Green";
            thislist = GetAnimationsPerColour(parameter, thislist);
            parameter = "Orange";
            thislist = GetAnimationsPerColour(parameter, thislist);
            parameter = "Pink";
            thislist = GetAnimationsPerColour(parameter, thislist);
            parameter = "PacMan";
            thislist = GetPacManMoves(thislist);

            //DeathAnimation

            Animation thisAnimation = new Animation();
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death1.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death1.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death1.png", UriKind.Relative))));

            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death2.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death2.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death2.png", UriKind.Relative))));

            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death3.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death3.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death3.png", UriKind.Relative))));

            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death4.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death4.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death4.png", UriKind.Relative))));

            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death5.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death5.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death5.png", UriKind.Relative))));

            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death6.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death6.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death6.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}Death6.png", UriKind.Relative))));

            thislist.Add(thisAnimation);

            thislist = GetHansAnimation(thislist);

            return thislist;
        }

        // Adds to GetAnimations per Colour
        public List<Animation> GetAnimationsPerColour(string parameter, List<Animation> thislist)
        {
            Animation thisAnimation = GetUpAnimation(parameter);
            thislist.Add(thisAnimation);
            thisAnimation = GetRightAnimation(parameter);
            thislist.Add(thisAnimation);
            thisAnimation = GetDownAnimation(parameter);
            thislist.Add(thisAnimation);
            thisAnimation = GetLeftAnimation(parameter);
            thislist.Add(thisAnimation);
            return thislist;
        }

        //Adds to GetAnimationPerColour per Direction * 4
        public Animation GetUpAnimation(string parameter)
        {
            Animation thisAnimation = new Animation();
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}GhostMoveUp1.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}GhostMoveUp2.png", UriKind.Relative))));
            return thisAnimation;
        }
        public Animation GetRightAnimation(string parameter)
        {
            Animation thisAnimation = new Animation();
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}GhostMoveRight1.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}GhostMoveRight2.png", UriKind.Relative))));
            return thisAnimation;
        }

        public Animation GetDownAnimation(string parameter)
        {
            Animation thisAnimation = new Animation();
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}GhostMoveDown1.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}GhostMoveDown2.png", UriKind.Relative))));
            return thisAnimation;
        }

        public Animation GetLeftAnimation(string parameter)
        {
            Animation thisAnimation = new Animation();
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}GhostMoveLeft1.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\{parameter}GhostMoveLeft2.png", UriKind.Relative))));
            return thisAnimation;
        }

        //Hans ghost animation
        public List<Animation> GetHansAnimation(List<Animation> thislist)
        {
            Animation thisAnimation = new Animation();
            thisAnimation.Frames.Add((new BitmapImage((new Uri(@"Images\HansGhost.png", UriKind.Relative)))));
            thislist.Add(thisAnimation);
            return thislist;
        }

        public List<Animation> GetPacManMoves(List<Animation> thislist)
        {
            Animation thisAnimation = new Animation();
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\PacManMoveUp1.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\PacManMoveUp2.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\PacManHasEaten.png", UriKind.Relative))));
            thislist.Add(thisAnimation);

            thisAnimation = new Animation();
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\PacManMoveRight1.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\PacManMoveRight2.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\PacManHasEaten.png", UriKind.Relative))));
            thislist.Add(thisAnimation);

            thisAnimation = new Animation();
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\PacManMoveDown1.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\PacManMoveDown2.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\PacManHasEaten.png", UriKind.Relative))));
            thislist.Add(thisAnimation);

            thisAnimation = new Animation();
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\PacManMoveLeft1.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\PacManMoveLeft2.png", UriKind.Relative))));
            thisAnimation.Frames.Add(new BitmapImage((new Uri($@"Images\PacManHasEaten.png", UriKind.Relative))));
            thislist.Add(thisAnimation);

            return thislist;
        }
    }
}
