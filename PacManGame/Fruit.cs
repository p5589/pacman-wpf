﻿using System.Windows;

namespace PacManGame
{
    class Fruit : Item, IConsumable, IStillSprite
    {
        public readonly int value = 200;
        public Fruit(Point location)
        {
            Location = location;
        }
        public string SpritePath { get; set; } = "Images/Cherry.png";
        public string ConsumeSfxPath { get; set; }
        public bool IsConsumed { get; set; }
        public void GetConsumed()
        {
            if (!IsConsumed)
            {
                IsConsumed = true;
                fruitConsumedEvent?.Invoke();
            }
        }
        public delegate void FruitConsumed();
        public static event FruitConsumed fruitConsumedEvent;
    }
}
