﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows;

namespace PacManGame
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        bool on = false;

        public User myUser = new User();

        public Login(User aUser)
        {
            InitializeComponent();
            myUser = aUser;
        }
        private void btnSignUpForm_Click(object sender, RoutedEventArgs e)
        {
            SignUpForm.Visibility = Visibility.Visible;
            SignInForm.Visibility = Visibility.Hidden;
            lblMessages.Content = "";
        }
        private void btnSignInForm_Click(object sender, RoutedEventArgs e)
        {
            SignUpForm.Visibility = Visibility.Hidden;
            SignInForm.Visibility = Visibility.Visible;
            lblMessages.Content = "";
        }
        private void btnSignIn_Click(object sender, RoutedEventArgs e)
        {
            //check if username not empty
            if (txtUsernameSignIn.Text != "")
            {
                // check if username exists
                if (DataManager.GetOneUser(txtUsernameSignIn.Text) != null)
                {
                    // get user object from existing username and compare passwords
                    User user = DataManager.GetOneUser(txtUsernameSignIn.Text);

                    if (user.Password == txtPasswordSignIn.Password)
                    {
                        EmptySignInFields();
                        lblMessages.Content = "";
                        this.DialogResult = true;
                        myUser = user;
                        this.Close();
                        Sound.StartMusic();

                    }
                    else
                    {
                        lblMessages.Content = $"The password for user '{user.Name}' is not correct.";
                    }
                }
                else
                {
                    lblMessages.Content = $"User '{txtUsernameSignUp.Text}' does not exist.";
                }
            }
            else
            {
                lblMessages.Content = $"Please fill in a username.";
            }

        }
        private void btnSignUp_Click(object sender, RoutedEventArgs e)
        {
            // check validation entered fields name/password
            string errors = StringInputValidation(txtUsernameSignUp.Text, txtPasswordSignUp.Password);
            errors += EmailInputValidation(txtEmail.Text);

            if (errors == "")
            {
                // check if username already exists
                if (DataManager.GetOneUser(txtUsernameSignUp.Text) == null)
                {
                    // make new user with fields and add to database + show message + clear fields sign up
                    int ok = DataManager.AddUser(txtUsernameSignUp.Text, txtPasswordSignUp.Password, txtEmail.Text);
                    if (ok > 0)
                    {
                        lblMessages.Content = $"New user '{txtUsernameSignUp.Text}' has been created.";
                        EmptySignUpFields();
                        SignUpForm.Visibility = Visibility.Hidden;
                        SignInForm.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        lblMessages.Content = $"Error: new user '{txtUsernameSignUp.Text}' could not be created.";
                    }
                }
                else
                {
                    lblMessages.Content = $"User '{txtUsernameSignUp.Text}' already exists.";
                }
            }
            else
            {
                lblMessages.Content = errors;
            }
        }
        private string StringInputValidation(string username, string password)
        {
            string errors = "";

            if (string.IsNullOrEmpty(username) || username.Length < 2 || username.Length > 50)
            {
                errors += "'Username' must contain minimum 2 and maximum 50 characters." + Environment.NewLine;
            }
            if (decimal.TryParse(username, out _))
            {
                errors += "'Username' cannot be a number." + Environment.NewLine;
            }
            if (string.IsNullOrEmpty(password) || password.Length < 2 || password.Length > 50)
            {
                errors += "'Password' must contain minimum 2 and maximum 50 characters." + Environment.NewLine;
            }
            return errors;
        }
        private string EmailInputValidation(string email)
        {
            string errors = "";

            if (string.IsNullOrEmpty(email))
            {
                errors += "Please fill in the 'Email' field." + Environment.NewLine;
            }
            else
            {
                if (!Regex.IsMatch(email, @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z"))
                {
                    errors += $"'{email}' is not a valid email address." + Environment.NewLine;
                }
            }
            return errors;
        }
        private void EmptySignUpFields()
        {
            txtUsernameSignUp.Text = null;
            txtPasswordSignUp.Password = null;
            txtEmail.Text = null;
        }
        private void EmptySignInFields()
        {
            txtUsernameSignIn.Text = null;
            txtPasswordSignIn.Password = null;
        }
        void Window_Closing(object sender, CancelEventArgs e)
        {
            //Application.Current.Shutdown();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Sound.StopMusic();
        }
    }
}
