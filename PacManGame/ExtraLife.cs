﻿using System.Windows;

namespace PacManGame
{
    class ExtraLife : Item, IConsumable, IStillSprite
    {
        public readonly int value = 1;
        public ExtraLife(Point location)
        {
            Location = location;
        }
        public string SpritePath { get; set; } = "Images/Crown.png";
        public string ConsumeSfxPath { get; set; }
        public bool IsConsumed { get; set; }
        public void GetConsumed()
        {
            if (!IsConsumed)
            {
                IsConsumed = true;
                extraLifeConsumedEvent?.Invoke();
            }
        }
        public delegate void ExtraLifeConsumed();
        public static event ExtraLifeConsumed extraLifeConsumedEvent;
    }
}
