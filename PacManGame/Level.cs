﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame
{
    class Level
    {
        public List<IConsumable> ConsumableItems { get; set; }

        public static List<int[,]> maps = new List<int[,]> { };


        public List<Enemy> Enemies { get; set; }
        public Player ThePlayer { get; set; }
        public int[,] Map { get; set; }

        public Level(int[,] map)
        {
            Map = map;
            ThePlayer = new Player();
        }
    }
}
