﻿namespace PacManGame
{
    public class _50PPAchievement : Achievement
    {
        public _50PPAchievement()
        {
            AchievementID = DataManager.GetAllAchievements()[2].AchievementID;
            Name = DataManager.GetAllAchievements()[2].Name;
            Description = DataManager.GetAllAchievements()[2].Description;
        }
        //Check 500 eaten dots
        public override void CheckIfConditionsAreMet(User user)
        {
            if (user.EatenPowerPellets == 50)
            {
                this.TriggerAchievement(user.UserID, this.AchievementID);
            }
        }
    }
}
