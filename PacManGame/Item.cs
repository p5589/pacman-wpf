﻿using System.Windows;

namespace PacManGame
{
    public abstract class Item : IGetsRendered
    {
        public Point Location { get; set; }

    }
}
