﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;

namespace PacManGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool on = false;
        bool btnStart = false;
        DispatcherTimer thisTimer = new DispatcherTimer();
        TimeSpan thisSpan;
        User thisUser = new User();
        bool checkDBConnection = false;

        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            MapDrawer.canvas = this.canvasPacman;
            UI.window = this;
            on = false;

            using (var myDBEntities = new PacmanDBEntities1())
            {
                checkDBConnection = myDBEntities.Database.Exists();
            }

            if (checkDBConnection)
            {
                //Call Login Screen On Load
                Login thisLogin = new Login(thisUser);
                thisLogin.ShowDialog();
                if (thisLogin.DialogResult.HasValue && thisLogin.DialogResult.Value)
                {
                    thisUser = thisLogin.myUser;
                    on = true;
                    lblUser.Content = $"Hello {thisUser.Name}!";

                    try
                    {
                        //Try to add score 0 for currentuser before asking top score as failsafe in case of no scores
                        DataManager.AddHighscore(thisUser.UserID, 0);
                        lblHighScore.Content = $"{DataManager.GetTopHighscoreUser(thisUser.UserID).Value}";

                        //Calls Main Menu Overlay
                        canvasPacman.Opacity = 0.2;
                        stkLifes.Opacity = 0.2;
                        stkScore.Opacity = 0.2;
                        stkHighscore.Opacity = 0.2;
                        stkCountDown.Visibility = Visibility.Hidden;
                        stkMainMenu.Visibility = Visibility.Visible;

                        //Button sound off/on
                        SoundOnOff();
                    }
                    catch (Exception ex)
                    {
                        //Fail silently
                    }
                }
                else
                {
                    this.Close();
                }

            }
            else
            {
                MessageBox.Show("Could not connect to the Database 'PacmanDB' on server '.\\SQLEXPRESS'.\nPlease check that the Database exists.", "Database Connection Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }
        }

        private void btnStartGame_Click(object sender, RoutedEventArgs e)
        {
            btnStart = true;

            // Calls & Hides CountdownTimer/Mainmenu
            stkTop15HighScores.Visibility = Visibility.Hidden;
            stkMainMenu.Visibility = Visibility.Hidden;
            imgMenu1.Visibility = Visibility.Hidden;
            imgMenu2.Visibility = Visibility.Hidden;
            imgMenu3.Visibility = Visibility.Hidden;
            stkCountDown.Visibility = Visibility.Visible;
            btnExitGame.Visibility = Visibility.Visible;
            thisTimer.Interval = TimeSpan.FromSeconds(1);
            thisSpan = TimeSpan.FromSeconds(5);
            thisTimer.Tick += UpdateCounter;
            thisTimer.Start();

            //TimeSpan thisSpan = new TimeSpan();
            //thisSpan = TimeSpan.FromSeconds(5);
            //thisTimer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            //{
            //   
            //    if (thisSpan == TimeSpan.Zero)
            //    {
            //        stkCountDown.Visibility = Visibility.Hidden;
            //        canvasPacman.Opacity = 1;
            //        stkLifes.Opacity = 1;
            //        stkScore.Opacity = 1;
            //        stkHighscore.Opacity = 1;
            //        lblTimer.Content = "";
            //        //Starts Actual Game
            //        canvasPacman.Visibility = Visibility.Visible;
            //        GameManager.Start(this.canvasPacman);
            //    }
            //    thisSpan = thisSpan.Add(TimeSpan.FromSeconds(-1));
            //}, Application.Current.Dispatcher);
            //thisTimer.Start();
            UI.GetUserHighScore(thisUser);
        }

        private void btnHighScores_Click(object sender, RoutedEventArgs e)
        {
            Highscores thisHighscore = new Highscores(thisUser.UserID);
            thisHighscore.Owner = this;
            thisHighscore.ShowDialog();
        }

        private void btnAbout_Click(object sender, RoutedEventArgs e)
        {
            About thisAbout = new About();
            thisAbout.Owner = this;
            thisAbout.ShowDialog();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            Login thisLogin = new Login(thisUser);
            thisLogin.Owner = this;
            thisLogin.ShowDialog();
            if (thisLogin.DialogResult.HasValue && thisLogin.DialogResult.Value)
            {
                thisUser = thisLogin.myUser;
            }
            else
            {
                this.Close();
            }
            lblUser.Content = $"Hello {thisUser.Name}!";
            lblHighScore.Content = $"{DataManager.GetTopHighscoreUser(thisUser.UserID).Value}";

        }

        private void btnSoundOnOff_Click(object sender, RoutedEventArgs e)
        {
            SoundOnOff();
        }

        private void SoundOnOff()
        {
            if (on)
            {
                btnSoundOnOff.Content = "Sound off";
                Sound.Unmute();
                on = false;
            }
            else
            {
                btnSoundOnOff.Content = "Sound on";
                Sound.Mute();
                on = true;
            }
        }

        private void btnBackToMenu_Click(object sender, RoutedEventArgs e)
        {

            //stkTop15HighScores.Visibility = Visibility.Hidden;

            thisTimer.Tick -= UpdateCounter;
            lblTimer.Content = TimeSpan.FromSeconds(5).ToString();
            GameManager.HandleDataBase();
            GameManager.UnsubscribeFromEvents();
            canvasPacman.Opacity = 0.2;
            stkLifes.Opacity = 0.2;
            stkScore.Opacity = 0.2;
            stkHighscore.Opacity = 0.2;
            stkCountDown.Visibility = Visibility.Hidden;
            stkTop15HighScores.Visibility = Visibility.Hidden;
            canvasPacman.Visibility = Visibility.Hidden;
            stkMainMenu.Visibility = Visibility.Visible;
            imgMenu1.Visibility = Visibility.Visible;
            imgMenu2.Visibility = Visibility.Visible;
            imgMenu3.Visibility = Visibility.Visible;
            btnExitGame.Visibility = Visibility.Hidden;
        }

        private void btnMapmaker_Click(object sender, RoutedEventArgs e)
        {

            MapMaker thisMapMaker = new MapMaker();
            thisMapMaker.Owner = this;
            thisMapMaker.ShowDialog();
            if (thisMapMaker.DialogResult.HasValue && thisMapMaker.DialogResult.Value)
            {
                MessageBox.Show("Succes, Map Loaded Next GameSession", "Hopla", MessageBoxButton.OK);
            }
        }

        private void UpdateCounter(object sender, EventArgs e)
        {
            thisSpan = thisSpan.Add(TimeSpan.FromSeconds(-1));
            lblTimer.Content = thisSpan.ToString();
            if (thisSpan == TimeSpan.Zero)
            {
                thisTimer.Stop();
                thisTimer.Tick -= UpdateCounter;
                stkCountDown.Visibility = Visibility.Hidden;
                canvasPacman.Opacity = 1;
                stkLifes.Opacity = 1;
                stkScore.Opacity = 1;
                stkHighscore.Opacity = 1;
                lblTimer.Content = "";
                //Starts Actual Game
                canvasPacman.Visibility = Visibility.Visible;
                GameManager.Start(canvasPacman, thisUser);
                Sound.StartMusic();
            }
        }
    }
}
