﻿namespace PacManGame
{
    public class HansGhost : Enemy, IAnimatable
    {
        private int _currentFrame = 0;



        public HansGhost()
        {
            AttackPower = 2;
            Direction = new System.Windows.Point(1, 0);
            value = -50;
        }


        public int CurrentFrame
        {
            get { return _currentFrame; }
            set { _currentFrame = value; }
        }

        public override void TakeDamage(int amount)
        {
            base.TakeDamage(amount);
            Spawn();
        }

        public Animation Animation
        {
            get
            {
                Animation thisAnimation = new Animation();
                if (Direction.X == 1)
                {
                    thisAnimation = thisAnimation.Dictionary[$"HansGhost"];
                }
                else if (Direction.X == -1)
                {
                    thisAnimation = thisAnimation.Dictionary[$"HansGhost"];
                }
                else if (Direction.Y == -1)
                {
                    thisAnimation = thisAnimation.Dictionary[$"HansGhost"];
                }
                else if (Direction.Y == 1)
                {
                    thisAnimation = thisAnimation.Dictionary[$"HansGhost"];
                }
                return thisAnimation;
            }
        }
    }
}

