﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame
{
    public static class MapSpecification
    {

        public static int TileSize
        {
            get { return 20; } //best to keep even
        }

        public static Point MapSize
        {
            get { return new Point() { X = 20, Y = 20 }; }
        }
    }
}
