﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PacManGame
{
    /// <summary>
    /// Interaction logic for MapMaker.xaml
    /// </summary>   
    public partial class MapMaker : Window
    {

        DispatcherTimer timer = new DispatcherTimer();
        int sizeBlock = 20;
        int blocksHorizontal = 20;
        int blocksVertical = 20;
        int[,] map;
        bool sound = false;


        public MapMaker()
        {
            InitializeComponent();

            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            Background = new ImageBrush(new BitmapImage(new Uri(@"Images\backgroundMapMaker.jpg", UriKind.Relative)));
            Background.Opacity = 0.8;
            //Fills Cmb
            cmbMapSelection.Items.Add("Map1");
            cmbMapSelection.Items.Add("Map2");
            cmbMapSelection.Items.Add("Map3");

            btnSave.MouseRightButtonDown += new MouseButtonEventHandler(btnSave_MouseRightButtonDown);

            cmbTileSelection.Items.Add("Wall 200<X<251 -- Blue");
            cmbTileSelection.Items.Add("Dot 0<X<201 -- Pink");
            cmbTileSelection.Items.Add("PowerPellet Max #9 -- Yellow");
            cmbTileSelection.Items.Add("Player Spawn Point Exact #1 -- Purple");
            cmbTileSelection.Items.Add("Ghost Spawn Point  0<X<7 -- Gray");
            cmbTileSelection.Items.Add("HansGhost Exact #1-- Red");
            cmbTileSelection.Items.Add("Fruit Max #3 -- Green");
            cmbTileSelection.Items.Add("ExtraLife Max #2-- Orange");

            //Start ticker for Rendering + Render Base
            timer.Tick += UpdateEveryFrame;
            timer.Interval = TimeSpan.FromMilliseconds(10);
            timer.Start();

            map = new int[blocksVertical, blocksHorizontal];
            var b = new SolidColorBrush(Colors.Blue);
            var w = new SolidColorBrush(Colors.White);
            for (int i = 0; i < blocksVertical; i++)
            {
                for (int j = 0; j < blocksHorizontal; j++)
                {
                    Shape turretBarrel = new Rectangle { Width = sizeBlock, Height = sizeBlock, Fill = b };
                    Canvas.SetLeft(turretBarrel, i * sizeBlock);
                    Canvas.SetTop(turretBarrel, j * sizeBlock);
                    canvasMapMaker.Children.Add(turretBarrel);
                    map[i, j] = 1;
                }
            }
        }

        //Updates Map/tick + Registers Mouseclicks in Array
        private void UpdateEveryFrame(object sender, EventArgs e)
        {
            if (Mouse.RightButton == MouseButtonState.Pressed && isInsideCanvas())
            {
                int column = (int)(Mouse.GetPosition(canvasMapMaker).X / sizeBlock);
                int row = (int)(Mouse.GetPosition(canvasMapMaker).Y / sizeBlock);
                if ((column != 0) && (column != 19) && (row != 0) && (row != 19))
                {
                    map[row, column] = 0;
                }
                RedrawMap();
            }
            if (Mouse.LeftButton == MouseButtonState.Pressed && isInsideCanvas())
            {
                switch (cmbTileSelection.SelectedIndex)
                {
                    case 0:
                        int column = (int)(Mouse.GetPosition(canvasMapMaker).X / sizeBlock);
                        int row = (int)(Mouse.GetPosition(canvasMapMaker).Y / sizeBlock);
                        if ((column != 0) && (column != 19) && (row != 0) && (row != 19))
                        {
                            map[row, column] = 1;
                        }
                        break;
                    case 1:
                        column = (int)(Mouse.GetPosition(canvasMapMaker).X / sizeBlock);
                        row = (int)(Mouse.GetPosition(canvasMapMaker).Y / sizeBlock);
                        if ((column != 0) && (column != 19) && (row != 0) && (row != 19))
                        {
                            map[row, column] = 2;

                        }
                        break;
                    case 2:
                        column = (int)(Mouse.GetPosition(canvasMapMaker).X / sizeBlock);
                        row = (int)(Mouse.GetPosition(canvasMapMaker).Y / sizeBlock);
                        if ((column != 0) && (column != 19) && (row != 0) && (row != 19))
                        {
                            map[row, column] = 3;
                        }
                        break;
                    case 3:
                        column = (int)(Mouse.GetPosition(canvasMapMaker).X / sizeBlock);
                        row = (int)(Mouse.GetPosition(canvasMapMaker).Y / sizeBlock);
                        if ((column != 0) && (column != 19) && (row != 0) && (row != 19))
                        {
                            map[row, column] = 4;
                        }
                        break;
                    case 4:
                        column = (int)(Mouse.GetPosition(canvasMapMaker).X / sizeBlock);
                        row = (int)(Mouse.GetPosition(canvasMapMaker).Y / sizeBlock);
                        if ((column != 0) && (column != 19) && (row != 0) && (row != 19))
                        {
                            map[row, column] = 5;
                        }
                        break;
                    case 5:
                        column = (int)(Mouse.GetPosition(canvasMapMaker).X / sizeBlock);
                        row = (int)(Mouse.GetPosition(canvasMapMaker).Y / sizeBlock);
                        if ((column != 0) && (column != 19) && (row != 0) && (row != 19))
                        {
                            map[row, column] = 8;
                        }
                        break;
                    case 6:
                        column = (int)(Mouse.GetPosition(canvasMapMaker).X / sizeBlock);
                        row = (int)(Mouse.GetPosition(canvasMapMaker).Y / sizeBlock);
                        if ((column != 0) && (column != 19) && (row != 0) && (row != 19))
                        {
                            map[row, column] = 6;
                        }
                        break;
                    case 7:
                        column = (int)(Mouse.GetPosition(canvasMapMaker).X / sizeBlock);
                        row = (int)(Mouse.GetPosition(canvasMapMaker).Y / sizeBlock);
                        if ((column != 0) && (column != 19) && (row != 0) && (row != 19))
                        {
                            map[row, column] = 7;
                        }
                        break;
                    default:
                        break;
                }
                RedrawMap();
            }
        }
        //Checks if valid <-> Canvas
        private bool isInsideCanvas()
        {
            return Mouse.GetPosition(canvasMapMaker).Y > 0
                 && Mouse.GetPosition(canvasMapMaker).Y < canvasMapMaker.Height
                 && Mouse.GetPosition(canvasMapMaker).X > 0
                  && Mouse.GetPosition(canvasMapMaker).X < canvasMapMaker.Width;
        }

        private void btnMakeMap_Click(object sender, RoutedEventArgs e)
        {
            txtOutputMapMaker.Text = ConvertMapArrayToString();
        }


        //Map to string for Dev Use
        private void MapToString()
        {
            string mapstring = "{{";
            for (int i = 0; i < map.GetLength(0); i++)
            {
                if (mapstring != "{{")
                {
                    mapstring += "," + Environment.NewLine + "{";
                }
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    mapstring += map[i, j];
                    if (j != map.GetLength(1) - 1)
                    {
                        mapstring += ",";
                    }
                }
                mapstring += "}";
            }
            mapstring += "}";
            Clipboard.SetText(mapstring);
        }

        //Generates map for export in propper format + checks conditions
        private string ConvertMapArrayToString()
        {
            btnSave.IsEnabled = false;
            btnSave.Visibility = Visibility.Hidden;
            string mapstring = "";
            int wallcount = 0;
            int playercount = 0;
            int ghostcount = 0;
            int dotcount = 0;
            int ppcount = 0;
            int lifecount = 0;
            int fruitcount = 0;
            int hanscount = 0;
            for (int i = 0; i < blocksVertical; i++)
            {
                for (int j = 0; j < blocksHorizontal; j++)
                {
                    switch (map[i, j])
                    {
                        case 1:
                            wallcount++;
                            break;
                        case 2:
                            dotcount++;
                            break;
                        case 3:
                            ppcount++;
                            break;
                        case 4:
                            playercount++;
                            break;
                        case 5:
                            ghostcount++;
                            break;
                        case 6:
                            fruitcount++;
                            break;
                        case 7:
                            lifecount++;
                            break;
                        case 8:
                            hanscount++;
                            break;
                        default:
                            break;
                    }
                }
            }
            if (wallcount < 200 || wallcount > 250)
            {
                MessageBox.Show($"Map contains {wallcount} walls, min is 200, max is 250", "Ah ah ah", MessageBoxButton.OK);
            }
            else if (dotcount > 200 || dotcount < 1)
            {
                MessageBox.Show($"Map contains {dotcount} dots, max is 200, minimum 1.", "Ah ah ah", MessageBoxButton.OK);
            }
            else if (ppcount > 9)
            {
                MessageBox.Show($"Map contains {ppcount} PowerPellets, max is 9", "Ah ah ah", MessageBoxButton.OK);
            }
            else if (playercount != 1)
            {
                MessageBox.Show($"Map contains {playercount} Players, It has to be exact 1", "Ah ah ah", MessageBoxButton.OK);
            }
            else if (ghostcount > 6 || ghostcount < 1)
            {
                MessageBox.Show($"Map contains {ghostcount} Ghosts, min 1, max 6", "Ah ah ah", MessageBoxButton.OK);
            }
            else if (hanscount != 1)
            {
                MessageBox.Show($"Map contains {hanscount} HansGhosts, Exact 1!", "Ah ah ah", MessageBoxButton.OK);
            }
            else if (fruitcount > 4)
            {
                MessageBox.Show($"Map contains {fruitcount} Fruits, It has to less then 5", "Ah ah ah", MessageBoxButton.OK);
            }
            else if (lifecount > 2)
            {
                MessageBox.Show($"Map contains {lifecount} Lives, It has to less then 3", "Ah ah ah", MessageBoxButton.OK);
            }
            else
            {
                mapstring = $"-------------Map Rendered Succesfull-------------\nWallCount = 200/->{wallcount}<-/250 -- DotCount = 0<{dotcount}<200\n" +
                    $"PowerpelletCount = {ppcount}/9 -- FruitCount = {fruitcount}/4\nLifeCount = {lifecount}/2. Ghosts = 0<{ghostcount}<7\nDesign is VALID!\nPress -Export to Game- to finish!";
                btnSave.IsEnabled = true;
                btnSave.Visibility = Visibility.Visible;
            }
            return mapstring;
        }

        private void btnLoadMap_Click(object sender, RoutedEventArgs e)
        {
            if (cmbMapSelection.SelectedIndex != -1)
            {

                if (MessageBox.Show("Do you want to load this Map? Your Current map will be lost!!", "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    map = GetMap();
                    RedrawMap();
                }
                cmbMapSelection.SelectedIndex = -1;
            }

        }
        // for when i needs to be j XD
        public int GetIndexWithSwappedRowsAndColumns(int a)
        {
            var column = GetColumn(a);
            var row = GetRow(a);
            return row * blocksHorizontal + column;
        }

        private int GetRow(int a)
        {
            return (int)(a % blocksHorizontal);
        }

        private int GetColumn(int a)
        {
            return (int)(a / blocksHorizontal);
        }

        private void btnAbort_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {

            btnSave.MouseRightButtonDown -= new MouseButtonEventHandler(btnSave_MouseRightButtonDown);
            GameManager.customMap = map;
            this.DialogResult = true;
        }

        //Renders map on tick
        public void RedrawMap()
        {
            btnSave.Visibility = Visibility.Hidden;
            canvasMapMaker.Children.Clear();
            var b = new SolidColorBrush(Colors.Blue);
            var d = new SolidColorBrush(Colors.Pink);
            var pp = new SolidColorBrush(Colors.Yellow);
            var p = new SolidColorBrush(Colors.Purple);
            var g = new SolidColorBrush(Colors.Gray);
            var h = new SolidColorBrush(Colors.Red);
            var f = new SolidColorBrush(Colors.Green);
            var l = new SolidColorBrush(Colors.Orange);
            var w = new SolidColorBrush(Colors.White);
            for (int i = 0; i < blocksVertical; i++)
            {
                for (int j = 0; j < blocksHorizontal; j++)
                {
                    switch (map[j, i])
                    {
                        case 0:
                            ColourPainter(w, i, j);
                            break;
                        case 1:
                            ColourPainter(b, i, j);
                            break;
                        case 2:
                            ColourPainter(d, i, j);
                            break;
                        case 3:
                            ColourPainter(pp, i, j);
                            break;
                        case 4:
                            ColourPainter(p, i, j);
                            break;
                        case 5:
                            ColourPainter(g, i, j);
                            break;
                        case 6:
                            ColourPainter(f, i, j);
                            break;
                        case 7:
                            ColourPainter(l, i, j);
                            break;
                        case 8:
                            ColourPainter(h, i, j);
                            break;
                    }
                }
            }
        }

        //Actual "Paint" event + add to canvs
        public void ColourPainter(SolidColorBrush x, int i, int j)
        {
            Shape turretBarrel = new Rectangle { Width = sizeBlock, Height = sizeBlock, Fill = x };
            Canvas.SetLeft(turretBarrel, i * sizeBlock);
            Canvas.SetTop(turretBarrel, j * sizeBlock);
            canvasMapMaker.Children.Add(turretBarrel);
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            txtOutputMapMaker.Text = $"If a block is blue, it is a wall.\nTo make an empty tile,\nhold right-click, it will be white.\nTo add a custom tile,\nhold left-click.";
        }

        private void btnSound_Click(object sender, RoutedEventArgs e)
        {
            if (sound)
            {
                Sound.Mute();
                sound = false;
            }
            else
            {
                sound = true;
                Sound.Unmute();
            }
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Do you want to reset to the Original Map? Your Current map will be lost!!", "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                canvasMapMaker.Children.Clear();
                var b = new SolidColorBrush(Colors.Blue);
                var w = new SolidColorBrush(Colors.White);
                for (int i = 0; i < blocksVertical; i++)
                {
                    for (int j = 0; j < blocksHorizontal; j++)
                    {
                        Shape turretBarrel = new Rectangle { Width = sizeBlock, Height = sizeBlock, Fill = b };
                        Canvas.SetLeft(turretBarrel, i * sizeBlock);
                        Canvas.SetTop(turretBarrel, j * sizeBlock);
                        canvasMapMaker.Children.Add(turretBarrel);
                        map[i, j] = 1;
                    }
                }
            }
        }

        //Copy Map from GameManager for further use
        private int[,] GetMap()
        {
            int[,] loadedmap = new int[20, 20];
            switch (cmbMapSelection.SelectedIndex)
            {
                case 0:
                    Array.Copy(GameManager.mapLevel1, loadedmap, 400);
                    break;
                case 1:
                    Array.Copy(GameManager.mapLevel2, loadedmap, 400);
                    break;
                case 2:
                    Array.Copy(GameManager.mapLevel3, loadedmap, 400);
                    break;
            }
            return loadedmap;
        }
        public void btnSave_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            MapToString();
            MessageBox.Show("MapArray Copied to ClipBoard", "Succes", MessageBoxButton.OK);
        }
    }
}
