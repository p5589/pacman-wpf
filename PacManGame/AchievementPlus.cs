﻿namespace PacManGame
{
    public partial class Achievement
    {
        //Condition to check whether achievement has been met
        public virtual void CheckIfConditionsAreMet(User user)
        {
        }


        //trigger the achievement
        public void TriggerAchievement(int userid, int achievementid)
        {
            DataManager.GainNewAchievement(userid, achievementid);
        }
    }
}
