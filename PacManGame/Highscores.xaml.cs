﻿using System.Collections.Generic;
using System.Windows;

namespace PacManGame
{
    /// <summary>
    /// Interaction logic for Highscores.xaml
    /// </summary>
    public partial class Highscores : Window
    {
        public Highscores(int userid)
        {
            InitializeComponent();

            //fill in list of top 15 highscores (highscore tab)
            List<Highscore> listHighscores = DataManager.GetTop15Highscores();
            int i = 1;
            foreach (Highscore hs in listHighscores)
            {
                hs.Rank = i;

                //fill in color property based on rank in top 15 list
                switch (hs.Rank)
                {
                    case 1:
                    case 6:
                    case 11:
                        hs.Color = "Red";
                        break;
                    case 2:
                    case 7:
                    case 12:
                        hs.Color = "Pink";
                        break;
                    case 3:
                    case 8:
                    case 13:
                        hs.Color = "Aqua";
                        break;
                    case 4:
                    case 9:
                    case 14:
                        hs.Color = "Orange";
                        break;
                    case 5:
                    case 10:
                    case 15:
                        hs.Color = "Yellow";
                        break;
                }
                i++;
            }

            icHighscores.ItemsSource = listHighscores;

            //check user gained achievements and check correct achievement box
            //(list achievements hardcoded in xaml)

            List<Achievement> userAchievements = DataManager.GetUserGainedAchievements(userid);

            if (userAchievements != null)
            {
                foreach (Achievement ach in userAchievements)
                {
                    switch (ach.AchievementID)
                    {
                        case 1:
                            chkAchievement1.IsChecked = true;
                            break;
                        case 2:
                            chkAchievement2.IsChecked = true;
                            break;
                        case 3:
                            chkAchievement3.IsChecked = true;
                            break;
                        case 4:
                            chkAchievement4.IsChecked = true;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
