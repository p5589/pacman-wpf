﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace PacManGame
{
    public class DataManager
    {
        //Allow access to controls from MainWindow in this class
        static MainWindow Form = Application.Current.Windows[0] as MainWindow;

        // USER METHODS

        // Add new user to database
        public static int AddUser(string username, string password, string email)
        {
            int result = 0;
            try
            {
                // Make new user object with info from login screen & set eatendots to 0
                User newUser = new User();
                newUser.Name = username;
                newUser.Password = password;
                newUser.Email = email;
                newUser.EatenDots = 0;
                newUser.EatenPowerPellets = 0;
                newUser.EatenGhosts = 0;
                Highscore myHighscore = new Highscore();
                myHighscore.Value = 0;
                myHighscore.Date = DateTime.Now;
                newUser.Highscores.Add(myHighscore);
                using (var myDBEntities = new PacmanDBEntities1())
                {
                    myDBEntities.Users.Add(newUser);

                    if (0 < myDBEntities.SaveChanges())
                    {
                        result = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred while retrieving data from the database.\n\nError Message:\n{ex}", "Database Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return result;
        }

        // Modify existing user in database
        public static int ModifyUser(User user)
        {
            int i = 0;
            try
            {
                using (var myDBEntities = new PacmanDBEntities1())
                {
                    myDBEntities.Entry(user).State = System.Data.Entity.EntityState.Modified;

                    i = myDBEntities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred while retrieving data from the database.\n\nError Message:\n{ex}", "Database Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return i;
        }

        // Get alphabetical list with all users in database
        public static List<User> GetAllUsers()
        {
            List<User> listUsers = new List<User>();
            try
            {
                using (var myDBEntities = new PacmanDBEntities1())
                {
                    var query = from user in myDBEntities.Users
                                orderby user.Name
                                select user;

                    foreach (User u in query)
                    {
                        listUsers.Add(u);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred while retrieving data from the database.\n\nError Message:\n{ex}", "Database Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return listUsers;
        }

        // Find a certain user in database by username  -  CAN BE NULL !
        public static User GetOneUser(string username)
        {
            User u = new User();
            try
            {
                using (var myDBEntities = new PacmanDBEntities1())
                {
                    var query = from user in myDBEntities.Users
                                where user.Name == username
                                select user;

                    u = query.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred while retrieving data from the database.\n\nError Message:\n{ex}", "Database Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return u;
        }

        // Find a certain user in database by userid  -  CAN BE NULL !
        public static User GetOneUser(int userid)
        {
            User u = new User();
            try
            {
                using (var myDBEntities = new PacmanDBEntities1())
                {
                    var query = from user in myDBEntities.Users
                                where user.UserID == userid
                                select user;

                    u = query.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred while retrieving data from the database.\n\nError Message:\n{ex}", "Database Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return u;
        }


        // ACHIEVEMENT METHODS

        // Get a list of all existing achievements
        public static List<Achievement> GetAllAchievements()
        {
            List<Achievement> listAchievements = new List<Achievement>();
            try
            {
                using (var myDBEntities = new PacmanDBEntities1())
                {
                    var query = from achievement in myDBEntities.Achievements
                                select achievement;

                    foreach (Achievement a in query)
                    {
                        listAchievements.Add(a);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred while retrieving data from the database.\n\nError Message:\n{ex}", "Database Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return listAchievements;
        }

        // Get a list of achievements that user has gained  - CAN BE NULL !
        public static List<Achievement> GetUserGainedAchievements(int userid)
        {
            List<Achievement> listAchievements = new List<Achievement>();
            try
            {
                using (var myDBEntities = new PacmanDBEntities1())
                {
                    var query = from userachievement in myDBEntities.UsersAchievements
                                .Include("User")
                                .Include("Achievement")
                                where userachievement.UserID == userid
                                select userachievement;

                    foreach (var userachievement in query)
                    {
                        listAchievements.Add(userachievement.Achievement);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred while retrieving data from the database.\n\nError Message:\n{ex}", "Database Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return listAchievements;
        }

        // Get a list of achievements that user has not yet gained  -  CAN BE NULL !
        public static List<Achievement> GetUserMissingAchievements(int userid)
        {
            List<Achievement> listAllAchievements = GetAllAchievements();
            List<Achievement> listUserAchievements = GetUserGainedAchievements(userid);
            List<Achievement> listUserMissingAchievements = new List<Achievement>();

            if (listUserAchievements != null)
            {
                foreach (Achievement achievement in listAllAchievements)
                {
                    bool match = false;
                    foreach (Achievement userachievement in listUserAchievements)
                    {
                        if (achievement.AchievementID == userachievement.AchievementID)
                        {
                            match = true;
                        }
                    }
                    if (match == false)
                    {
                        listUserMissingAchievements.Add(achievement);
                    }
                }
            }
            else
            {
                listUserMissingAchievements = listAllAchievements;
            }
            return listUserMissingAchievements;
        }

        // Add new gained achievement for user in usersachievements (only when user has not yet gained that achievement)
        // method returns "1" only when userachievement is actually added to the database
        public static int GainNewAchievement(int userid, int achievementid)
        {
            int result = 0;
            bool addNewAchievement = false;

            string achievementName = "";

            List<Achievement> usermissingachievements = GetUserMissingAchievements(userid);

            UserAchievement newUserAchievement = new UserAchievement();
            newUserAchievement.UserID = userid;
            newUserAchievement.AchievementID = achievementid;
            try
            {
                if (usermissingachievements != null)
                {
                    foreach (Achievement achievement in usermissingachievements)
                    {
                        if (achievement.AchievementID == achievementid)
                        {
                            addNewAchievement = true;
                            achievementName = achievement.Name;
                        }
                    }
                    if (addNewAchievement == true)
                    {
                        using (var myDBEntities = new PacmanDBEntities1())
                        {
                            myDBEntities.UsersAchievements.Add(newUserAchievement);

                            if (0 < myDBEntities.SaveChanges())
                            {
                                result = 1;

                                //Fill in popup with correct message and display
                                Form.popText.Text = achievementName;
                                Form.popAchievement1.IsOpen = true;
                                GameManager.AchievementPopUp = DateTime.Now;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred while retrieving data from the database.\n\nError Message:\n{ex}", "Database Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return result;
        }


        // HIGHSCORE METHODS

        // Add new highscore to database (only if new highscore is in top 15 OR is new personal highscore for the current user OR when user has no highscores yet)
        // method returns "1" only when a new highscore is actually added to the database
        public static int AddHighscore(int userid, int points)
        {
            int result = 0;
            List<Highscore> top15Highscores = GetTop15Highscores();
            List<Highscore> allHighscoresUser = GetAllHighscoresUser(userid);
            bool addNewHighscore = false;

            Highscore newHighscore = new Highscore();
            newHighscore.UserID = userid;
            newHighscore.Value = points;
            newHighscore.Date = DateTime.Now;

            //New highscore is added to DB when:
            // 1. when it is the first highscore for a user
            // 2. when new highscore is larger than the lowest highscore in top15 OR when there are less than 15 highscores 
            // -> but ONLY WHEN the user does not yet have that exact highscore
            try
            {
                if (allHighscoresUser.Count == 0)
                {
                    addNewHighscore = true;
                }
                else if (points > GetTopHighscoreUser(userid).Value)
                {
                    addNewHighscore = true;
                }
                else if (top15Highscores.Count < 15 || points > top15Highscores[top15Highscores.Count - 1].Value)
                {
                    bool checkUserHasHighscore = false;

                    foreach (Highscore hs in allHighscoresUser)
                    {
                        if (points == hs.Value)
                        {
                            checkUserHasHighscore = true;
                        }
                    }

                    if (checkUserHasHighscore == false)
                    {
                        addNewHighscore = true;
                    }
                }

                if (addNewHighscore == true)
                {
                    using (var myDBEntities = new PacmanDBEntities1())
                    {
                        myDBEntities.Highscores.Add(newHighscore);

                        if (0 < myDBEntities.SaveChanges())
                        {
                            result = 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred while retrieving data from the database.\n\nError Message:\n{ex}", "Database Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return result;
        }

        // Get a list of all highscores for a user  -  CAN BE NULL !
        public static List<Highscore> GetAllHighscoresUser(int userid)
        {
            List<Highscore> listHighscores = new List<Highscore>();
            try
            {
                using (var myDBEntities = new PacmanDBEntities1())
                {
                    var query = from highscore in myDBEntities.Highscores
                                where highscore.UserID == userid
                                orderby highscore.Value descending
                                select highscore;

                    foreach (Highscore hs in query)
                    {
                        listHighscores.Add(hs);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred while retrieving data from the database.\n\nError Message:\n{ex}", "Database Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return listHighscores;
        }

        // Get a list of the 15 best highscores  -  CAN BE NULL !
        public static List<Highscore> GetTop15Highscores()
        {
            List<Highscore> listHighscores = new List<Highscore>();
            try
            {
                using (var myDBEntities = new PacmanDBEntities1())
                {
                    var query = (from highscore in myDBEntities.Highscores
                                 where highscore.Value > 0
                                 orderby highscore.Value descending
                                 select highscore).Take(15);

                    foreach (Highscore hs in query)
                    {
                        listHighscores.Add(hs);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred while retrieving data from the database.\n\nError Message:\n{ex}", "Database Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return listHighscores;
        }

        // Get a user's best highscore  -  CAN BE NULL !
        public static Highscore GetTopHighscoreUser(int userid)
        {
            Highscore hs = new Highscore();
            try
            {
                using (var myDBEntities = new PacmanDBEntities1())
                {
                    var query = from highscore in myDBEntities.Highscores
                                where highscore.UserID == userid
                                orderby highscore.Value descending
                                select highscore;

                    hs = query.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred while retrieving data from the database.\n\nError Message:\n{ex}", "Database Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return hs;
        }
    }
}
