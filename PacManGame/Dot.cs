﻿using System.Windows;

namespace PacManGame
{
    public class Dot : Item, IConsumable, IStillSprite
    {
        public readonly int value = 10;
        public Dot(Point location)
        {
            Location = location;
        }
        public string SpritePath { get; set; } = "Images/Dot.png";
        public string ConsumeSfxPath { get; set; }
        public bool IsConsumed { get; set; }
        public void GetConsumed()
        {
            if (!IsConsumed)
            {
                IsConsumed = true;
                coinConsumedEvent?.Invoke();
            }
        }
        public delegate void CoinConsumed();
        public static event CoinConsumed coinConsumedEvent;
    }
}
