﻿using System;
using System.Diagnostics;
using System.Windows;

namespace PacManGame
{
    public abstract class Character : IGetsRendered
    {
        public int[,] Map { get { return GameManager.currentMap; } }
        public Point Location { get; set; }
        public Point OriginalLocation { get; set; }
        public Point Direction { get; set; }
        public int AttackPower { get; set; } = 1;
        public float Speed { get; set; } = 4; // best to keep even
        public int Lives
        {
            get { return lives; }
            set
            {
                lives = value;
                if (lives < 0)
                {
                    lives = 0;
                }
            }
        }
        public static Random rng = new Random();


        public string DieSfxPath;
        private int lives = 3;

        public void Spawn()
        {
            Location = OriginalLocation;
        }

        public void DoDamage(Character receiverDamage)
        {
            receiverDamage.TakeDamage(AttackPower);
        }

        public virtual void TakeDamage(int amount)
        {
            Lives -= amount;
            if (Lives <= 0)
            {
                Die();
            }
        }

        public void Move()
        {
            Location = new Point(Location.X + Direction.X * Speed, Location.Y + Direction.Y * Speed);
        }

        public virtual void Die()
        {
            DieEvent?.Invoke(DieSfxPath);
        }

        #region Moverelated code

        protected bool IsMoveInDirectionValid(Character character, Point desiredDirection)
        {
            //search for desired tile
            Point desiredTile = GetNextTileInDirection(character, desiredDirection);

            //Is desiredtile inside playing field check
            if (desiredTile.X < 0 || desiredTile.Y < 0 || desiredTile.X >= Map.GetLength(1) || desiredTile.Y >= Map.GetLength(1))
            {
                return false;
            }
            return Map[(int)(desiredTile.Y), (int)(desiredTile.X)] != 1;
        }


        protected Point GetNextTileInDirection(Character character, Point desiredDirection)
        {
            //TODO speed needs to be part of equation
            int x = (int)(character.Location.X);
            int y = (int)(character.Location.Y);
            if (character.Direction != desiredDirection)
            {
                x = (int)(ClosestTile(character.Location).X) * MapSpecification.TileSize;
                y = (int)(ClosestTile(character.Location).Y) * MapSpecification.TileSize;
            }
            if (desiredDirection.X == -1)
            {
                if (x % MapSpecification.TileSize == 0)
                {
                    x--;
                }
                return new Point(x / MapSpecification.TileSize, y / MapSpecification.TileSize);

            }
            else if (desiredDirection.X == 1)
            {
                return new Point(x / MapSpecification.TileSize + 1, y / MapSpecification.TileSize);
            }
            else if (desiredDirection.Y == -1)
            {
                if (y % MapSpecification.TileSize == 0)
                {
                    y--;
                }
                return new Point(x / MapSpecification.TileSize, y / MapSpecification.TileSize);
            }
            else if (desiredDirection.Y == 1)
            {
                return new Point(x / MapSpecification.TileSize, y / MapSpecification.TileSize + 1);
            }
            throw new ArgumentException();
        }

        protected Point ClosestTile(Point p)
        {
            return new Point(Math.Round(p.X / MapSpecification.TileSize), Math.Round(p.Y / MapSpecification.TileSize));
        }

        protected Point LocationToTileLocation(Point p)
        {
            return new Point((int)(p.X / MapSpecification.TileSize), (int)(p.Y / MapSpecification.TileSize));
        }
        protected bool ArePerpundicullar(Point p1, Point p2)
        {
            return (p1.X + p2.X) * (p1.Y + p2.Y) != 0;
        }

        #endregion


        public delegate void DieEventHandler(string method);
        public event DieEventHandler DieEvent;
    }
}
