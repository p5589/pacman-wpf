﻿namespace PacManGame
{
    public class _50GhostsAchievement : Achievement
    {
        public _50GhostsAchievement()
        {
            AchievementID = DataManager.GetAllAchievements()[3].AchievementID;
            Name = DataManager.GetAllAchievements()[3].Name;
            Description = DataManager.GetAllAchievements()[3].Description;
        }
        //Check 500 eaten dots
        public override void CheckIfConditionsAreMet(User user)
        {
            if (user.EatenGhosts == 50)
            {
                this.TriggerAchievement(user.UserID, this.AchievementID);
            }
        }
    }

}
