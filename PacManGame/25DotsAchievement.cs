﻿namespace PacManGame
{
    public class _25DotsAchievement : Achievement
    {
        public _25DotsAchievement()
        {
            AchievementID = DataManager.GetAllAchievements()[0].AchievementID;
            Name = DataManager.GetAllAchievements()[0].Name;
            Description = DataManager.GetAllAchievements()[0].Description;
        }
        //Check 25 eaten dots
        public override void CheckIfConditionsAreMet(User user)
        {
            if (user.EatenDots == 25)
            {
                this.TriggerAchievement(user.UserID, this.AchievementID);
            }
        }
    }

}
